package com.company;

import com.company.ui.console.BasicUI;
import com.company.ui.gui.MainWindow;

public class Main {

    /**
     * Uruchamia aplikację z graficznym (gui) lub konsolowym interfejsem użytkownika (nogui).
     *
     * @param args gui lub nogui
     */
    public static void main(String[] args) {
        if (args[0].equals("nogui"))
            new BasicUI();
        else
            new MainWindow();
    }
}
