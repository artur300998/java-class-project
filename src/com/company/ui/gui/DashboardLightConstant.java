package com.company.ui.gui;

import java.awt.*;

/**
 * Reprezentacja kontrolki świateł, która świeci stale po aktywowaniu.
 */
public class DashboardLightConstant extends DashboardLight {

    /**
     * Konstrukcja z użyciem klasy abstrakcyjnej {@link DashboardLight}.
     *
     * @param image obrazek, który będzie reprezentował stan kontrolki świateł
     */
    public DashboardLightConstant(Image image) {
        super(image);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (isLightUp)
            g.drawImage(image, 0, 0, dimension.width, dimension.height, this);
    }
}
