package com.company.ui.gui;

import com.company.logic.OnBoardComputerData;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Panel wyświetlający dane z komputera podkładowego.
 */
class OnBoardComputerPanel extends JPanel {

    /**
     * Element okna wyświetlający daną z komputera pokładowego.
     */
    private JLabel avgSpeedValueLabel, maxSpeedValueLabel, tripTimeValueLabel, distanceValueLabel, avgCombustionValueLabel;

    /**
     * Inicjalizacja panelu i jego komponentów.
     */
    public OnBoardComputerPanel() {

        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(new GridLayout(0, 3, 2, 5));

        initComponents();
    }

    /**
     * Inicjalizacja komponentów panelu.
     */
    private void initComponents() {
        add(new JLabel("Prędkość średnia"));
        avgSpeedValueLabel = new JLabel("0", SwingConstants.RIGHT);
        add(avgSpeedValueLabel);
        add(new JLabel("km/h"));

        add(new JLabel("Prędkość maks."));
        maxSpeedValueLabel = new JLabel("0", SwingConstants.RIGHT);
        add(maxSpeedValueLabel);
        add(new JLabel("km/h"));

        add(new JLabel("Czas podróży"));
        tripTimeValueLabel = new JLabel("0", SwingConstants.RIGHT);
        add(tripTimeValueLabel);
        add(new JLabel("min"));

        add(new JLabel("Dystans"));
        distanceValueLabel = new JLabel("0", SwingConstants.RIGHT);
        add(distanceValueLabel);
        add(new JLabel("km"));

        add(new JLabel("Średnie spalanie"));
        avgCombustionValueLabel = new JLabel("0", SwingConstants.RIGHT);
        add(avgCombustionValueLabel);
        add(new JLabel("l/100km"));
    }

    /**
     * Aktualizuje dane wyświetlane w panelu.
     *
     * @param onBoardComputerData dane z komputera podkładowego do wyświetlenia
     */
    public void updateOnBoardComputerData(OnBoardComputerData onBoardComputerData) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        avgSpeedValueLabel.setText(formatter.format(onBoardComputerData.avgSpeed));
        maxSpeedValueLabel.setText(formatter.format(onBoardComputerData.maxSpeed));
        tripTimeValueLabel.setText(Integer.toString(onBoardComputerData.tripTime));
        distanceValueLabel.setText(formatter.format(onBoardComputerData.distance));
        avgCombustionValueLabel.setText(formatter.format(onBoardComputerData.avgCombustion));
    }
}
