package com.company.ui.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Przechowuje wszystkie elementy (panele) okna.
 * Okno zawiera 5 paneli: panel radia, panel liczników, panel z kontrolkami świateł,
 * panel z danymi komputera pokładowego, panel z prędkościomierzem.
 */
class DashboardPanel extends JPanel {

    /**
     * Panel radia.
     */
    private AudioPlayerPanel audioPlayerPanel;
    /**
     * Panel z licznikami.
     */
    private CountersPanel countersPanel;
    /**
     * Panel z kontrolkami świateł.
     */
    private DashboardLightsPanel dashboardLightsPanel;
    /**
     * Panel z komputerem pokładowym.
     */
    private OnBoardComputerPanel onBoardComputerPanel;
    /**
     * Panel z prędkościomierzem.
     */
    private SpeedometerPanel speedometerPanel;

    /**
     * Inicjalizacja panelu i jego komponentów.
     */
    public DashboardPanel() {
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createLineBorder(Color.BLACK));

        initComponents();
    }

    /**
     * Inicjalizacja komponentów panelu.
     */
    private void initComponents() {
        onBoardComputerPanel = new OnBoardComputerPanel();
        dashboardLightsPanel = new DashboardLightsPanel();
        speedometerPanel = new SpeedometerPanel();
        countersPanel = new CountersPanel();
        audioPlayerPanel = new AudioPlayerPanel();

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.ipadx = 2;
        c.ipady = 2;
        c.insets = new Insets(2, 2, 2, 2);
        c.fill = GridBagConstraints.BOTH;

        c.anchor = GridBagConstraints.LINE_START;
        add(dashboardLightsPanel, c);
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_END;
        add(countersPanel, c);
        c.gridx = 0;
        c.gridy = 1;
        c.gridheight = 2;
        c.anchor = GridBagConstraints.LINE_START;
        add(speedometerPanel, c);
        c.gridx = 1;
        c.gridheight = 1;
        c.anchor = GridBagConstraints.LINE_END;
        add(onBoardComputerPanel, c);
        c.gridy = 2;
        c.anchor = GridBagConstraints.LINE_END;
        add(audioPlayerPanel, c);

        speedometerPanel.setPreferredSize(new Dimension(400, 400));

        onBoardComputerPanel.setVisible(true);
        dashboardLightsPanel.setVisible(true);
        speedometerPanel.setVisible(true);
        countersPanel.setVisible(true);
        audioPlayerPanel.setVisible(true);
    }

    /**
     * Zwraca panel zawierający elementy gui radia.
     *
     * @return panel audio
     */
    public AudioPlayerPanel getAudioPlayerPanel() {
        return audioPlayerPanel;
    }

    /**
     * Zwraca panel zawierający liczniki.
     *
     * @return panel z licznikami
     */
    public CountersPanel getCountersPanel() {
        return countersPanel;
    }

    /**
     * Zwraca panel z kontrolkami świateł.
     *
     * @return panel z kontrolkami świateł
     */
    public DashboardLightsPanel getDashboardLightsPanel() {
        return dashboardLightsPanel;
    }

    /**
     * Zwraca panel zawierający dane z komputera pokładowego.
     *
     * @return panel zawierający dane z komputera pokładowego
     */
    public OnBoardComputerPanel getOnBoardComputerPanel() {
        return onBoardComputerPanel;
    }

    /**
     * Zwraca panel zawierający prędkościomierz.
     *
     * @return panel zawierający prędkościomierz
     */
    public SpeedometerPanel getSpeedometerPanel() {
        return speedometerPanel;
    }

}
