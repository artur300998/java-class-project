package com.company.ui.gui;

import com.company.data.DataRepository;
import com.company.data.Resources;
import com.company.data.inout.ConfigManagerException;
import com.company.logic.AudioPlayer;
import com.company.logic.Dashboard;
import com.company.logic.Lights;
import com.company.logic.MainLogic;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Kontroler łączący warstwę logiki z warstwą interfejsu graficznego użytkownika.
 * Dokonuje odświeżeń elementów okna za pomocą metod każdego z paneli używając danych pozyskanych
 * z metod klas warstwy logiki.
 */
class GUIController {

    /**
     * Komendy radia
     */
    enum AudioPlayerCmd {
        PLAY,
        PAUSE,
        STOP,
        PREV,
        NEXT
    }

    /**
     * Deska rozdzielcza (warstwa logiki).
     */
    private Dashboard dashboard;
    /**
     * Główne okno.
     */
    private MainWindow mainWindow;

    /**
     * Obiekt obsługujący skróty klawiszowe.
     */
    private KeyBindings keyBindings;
    /**
     * Ustawienia graficzne prędkościomierza.
     */
    private GUISettings guiSettings;

    /**
     * Inicjalizacja kontrolera i GUI: ustawienie akcji dla przycisków, odczytanie ustawień i zastosowanie ich,
     * ustawienie skrotów klawiszowych, uruchomienie {@link Timer}, który odświeża GUI.
     *
     * @param mainWindow okno główne, którym ma sterować ten kontroler
     */
    public GUIController(MainWindow mainWindow) {
        dashboard = MainLogic.getInstance().getDashboard();
        this.mainWindow = mainWindow;

        setButtonsActions();
        setGuiSettings(mainWindow.getSettingsWindow().getGuiSettings());
        setKeyBindings();

        readFromXML(Resources.getURIToResource(".").resolve("config.xml").getPath());

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                updateGUI();
            }
        }, 0, 20);
    }

    /**
     * Pokazuje okno ustawień.
     */
    public void showSettingsWindow() {
        if (mainWindow.getSettingsWindow().isVisible())
            return;

        mainWindow.getSettingsWindow().setVisible(true);
    }

    /**
     * Pokazuje okno z klawiszologią.
     */
    public void showKeyBindingsWindow() {
        mainWindow.getKeyBindingsWindow().setText(keyBindings.toString());
        mainWindow.getKeyBindingsWindow().setVisible(true);
    }

    /**
     * Wywołuje zapis stanu aplikacji do pliku XML.
     *
     * @param filePath ścieżka do pliku XML
     */
    public void saveToXML(String filePath) {
        try {
            dashboard.getDataRepository().saveConfigurationToFile(filePath);
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wywołuje zapis stanu aplikacji do bazy danych.
     */
    public void saveToDB() {
        try {
            dashboard.getDataRepository().saveConfigurationToDatabase();
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wywołuje odczyt stanu aplikacji z pliku XML.
     *
     * @param filePath ścieżka do pliku XML
     */
    public void readFromXML(String filePath) {
        try {
            dashboard.getDataRepository().readConfigurationFromFile(filePath);
            setGuiSettings(dashboard.getDataRepository().getGuiSettings());
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wywołuje odczyt stanu aplikacji z bazy danych.
     */
    public void readFromDB() {
        try {
            dashboard.getDataRepository().readConfigurationFromDatabase();
            setGuiSettings(dashboard.getDataRepository().getGuiSettings());
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wywołuje odpowiednią funkcję radia dla podanej komendy.
     *
     * @param cmd komenda
     */
    public void performAudioPlayerCommand(AudioPlayerCmd cmd) {

        AudioPlayer ap = dashboard.getAudioPlayer();
        switch (cmd) {
            case PLAY:
                ap.playAudio();
                break;
            case PAUSE:
                ap.pauseAudio();
                break;
            case STOP:
                ap.stopAudio();
                break;
            case PREV:
                ap.prevSong();
                break;
            case NEXT:
                ap.nextSong();
                break;
        }
    }

    /**
     * Zapisuje stan aplikacji do pliku XML w domyślnej ścieżce przy wychodzeniu aplikacji.
     */
    public void exitTasks() {
        saveToXML(Resources.getURIToResource(".").resolve("config.xml").getPath());
    }

    /**
     * Ustawia akcje dla przycisków ze wszystkich panelów.
     */
    private void setButtonsActions() {
        mainWindow.getSettingsWindow().setApplyButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setGuiSettings(mainWindow.getSettingsWindow().getGuiSettings());
            }
        });

        AudioPlayerPanel app = mainWindow.getDashboardPanel().getAudioPlayerPanel();
        app.setPlayPauseButtonActions(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAudioPlayerCommand(AudioPlayerCmd.PLAY);
                app.switchPlayPauseButton(true);
            }
        }, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAudioPlayerCommand(AudioPlayerCmd.PAUSE);
                app.switchPlayPauseButton(false);
            }
        });
        app.setStopButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAudioPlayerCommand(AudioPlayerCmd.STOP);
                app.switchPlayPauseButton(false);
            }
        });
        app.setNextButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAudioPlayerCommand(AudioPlayerCmd.NEXT);
                app.switchPlayPauseButton(false);
            }
        });
        app.setPrevButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                performAudioPlayerCommand(AudioPlayerCmd.PREV);
                app.switchPlayPauseButton(false);
            }
        });

        mainWindow.getDashboardPanel().getCountersPanel().setZeroUserCounter1ButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getDataRepository().resetUserCounter1();
            }
        });
        mainWindow.getDashboardPanel().getCountersPanel().setZeroUserCounter2ButtonAction(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getDataRepository().resetUserCounter2();
            }
        });
    }

    /**
     * Aktualizuje wszystkie panele danymi z warstwy logiki.
     */
    private void updateGUI() {
        DashboardPanel dashboardPanel = mainWindow.getDashboardPanel();
        DataRepository dataRepository = dashboard.getDataRepository();

        dashboardPanel.getDashboardLightsPanel().updateLightsState(dashboard.getLights());

        dashboardPanel.getSpeedometerPanel().setMaxSpeed(dashboard.getMaxSpeed());
        dashboardPanel.getSpeedometerPanel().updateSpeed(dashboard.getSpeed());
        dashboardPanel.getSpeedometerPanel().updateGear(dashboard.getGear());
        dashboardPanel.getSpeedometerPanel().setCruiseControlActive(dashboard.isCruiseControlActive());

        dashboardPanel.getOnBoardComputerPanel().updateOnBoardComputerData(dashboard.getOnBoardComputerData());

        dashboardPanel.getCountersPanel().updateCounters(
                dataRepository.getTotalCounterValue(),
                dataRepository.getUserCounter1Value(),
                dataRepository.getUserCounter2Value()
        );

        AudioPlayer audioPlayer = dashboard.getAudioPlayer();
        dashboardPanel.getAudioPlayerPanel().updateSongTime(audioPlayer.getSongPercent());
        dashboardPanel.getAudioPlayerPanel().switchPlayPauseButton(dashboard.getAudioPlayer().isPlaying());
        dashboardPanel.getAudioPlayerPanel().updateTrackNameLabel(audioPlayer.getCurrentTrackName());
    }

    /**
     * Stosuje ustawienia.
     *
     * @param guiSettings ustawienia do zastosowania
     */
    private void setGuiSettings(GUISettings guiSettings) {
        this.guiSettings = guiSettings;
        mainWindow.getDashboardPanel().getSpeedometerPanel().updateSettings(guiSettings);
        mainWindow.getSettingsWindow().setGuiSettings(guiSettings);
        dashboard.getDataRepository().setSettings(guiSettings);
    }

    /**
     * Ustawia wszystkie skróty klawiszowe.
     */
    private void setKeyBindings() {
        keyBindings = new KeyBindings(mainWindow.getDashboardPanel());

        keyBindings.addKeyBinding("Przełacz światła (pozycyjne -> mijania -> drogowe)", "L", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().cycleHeadlightsState(true);
            }
        });

        keyBindings.addKeyBinding("Przełacz światła (drogowe -> mijania -> pozycyjne)", "K", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().cycleHeadlightsState(false);
            }
        });

        keyBindings.addKeyBinding("Przełacz światło przeciwmgielne przednie", "M", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().switchLightState(Lights.LightType.FOG_FRONT);
            }
        });

        keyBindings.addKeyBinding("Przełacz światło przeciwmgielne tylne", "N", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().switchLightState(Lights.LightType.FOG_BACK);
            }
        });

        keyBindings.addHoldKeyBinding("Skręt w lewo", "LEFT", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().enableIndicator(Lights.LightType.LEFT_INDICATOR);
            }
        }, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().disableIndicator(Lights.LightType.LEFT_INDICATOR);
            }
        });

        keyBindings.addHoldKeyBinding("Skręt w prawo", "RIGHT", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().enableIndicator(Lights.LightType.RIGHT_INDICATOR);
            }
        }, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.getLights().disableIndicator(Lights.LightType.RIGHT_INDICATOR);
            }
        });

        keyBindings.addHoldKeyBinding("Przyspieszanie", "UP", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.setAccel(Dashboard.Accel.UP);
            }
        }, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.setAccel(Dashboard.Accel.NOT_UP);
            }
        });

        keyBindings.addHoldKeyBinding("Hamowanie", "DOWN", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.setAccel(Dashboard.Accel.DOWN);
            }
        }, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.setAccel(Dashboard.Accel.NOT_DOWN);
            }
        });

        keyBindings.addKeyBinding("Aktywacja/dezaktywacja tempomatu", "T", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.switchCruiseControlActivation();
            }
        });

        keyBindings.addKeyBinding("Zwiększenie prędkości tempomatu", "PLUS", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.cruiseControlAddSpeed();
            }
        });

        keyBindings.addKeyBinding("Zwiększenie prędkości tempomatu", "ADD", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.cruiseControlAddSpeed();
            }
        });

        keyBindings.addKeyBinding("Zmniejszenie prędkości tempomatu", "MINUS", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.cruiseControlRemoveSpeed();
            }
        });

        keyBindings.addKeyBinding("Zmniejszenie prędkości tempomatu", "SUBTRACT", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dashboard.cruiseControlRemoveSpeed();
            }
        });
    }
}
