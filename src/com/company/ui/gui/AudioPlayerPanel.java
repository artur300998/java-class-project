package com.company.ui.gui;

import javax.swing.*;
import java.awt.*;

import static com.company.data.Resources.getImageFromResource;

/**
 * Panel zawierający elemnty GUI radia.
 */
class AudioPlayerPanel extends JPanel {

    /**
     * Zawiera nazwę aktualnie wskazywanego utworu w radiu.
     */
    private JTextArea trackNameText;
    /**
     * Pokazuje w sposób graficzny moment odtwarzania aktualnego utworu.
     */
    private JSlider slider;
    /**
     * Przycisk odtwarzacza.
     */
    private JButton playPauseButton, stopButton, prevButton, nextButton;

    /**
     * Ikona dla przycisku wznawiania/pauzowania.
     */
    private ImageIcon playIcon, pauseIcon;
    /**
     * Akcje dla przycisku wznawiania/pauzowania.
     */
    private Action playAction, pauseAction;

    /**
     * Inicjalizacja panelu i jego komponentów.
     */
    public AudioPlayerPanel() {

        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        initComponent();
    }

    /**
     * Inicjalizacja komponentów panelu.
     */
    private void initComponent() {
        trackNameText = new JTextArea("...");
        add(trackNameText);

        slider = new JSlider();
        slider.setMinimum(0);
        slider.setMaximum(100);
        slider.setFocusable(false);
        add(slider);

        playIcon = new ImageIcon(getImageFromResource("/player/play.png"));
        pauseIcon = new ImageIcon(getImageFromResource("/player/pause.png"));

        playPauseButton = new JButton(playIcon);
        stopButton = new JButton();
        prevButton = new JButton();
        nextButton = new JButton();

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(playPauseButton);
        buttonsPanel.add(stopButton);
        buttonsPanel.add(prevButton);
        buttonsPanel.add(nextButton);
        add(buttonsPanel);
    }

    /**
     * Ustawia akcje dla przycisku pauzy/wznawiania.
     *
     * @param playAction  akcja dla przycisku wznawiania
     * @param pauseAction akcja dla przycisku pauzy
     */
    public void setPlayPauseButtonActions(Action playAction, Action pauseAction) {
        this.playAction = playAction;
        this.pauseAction = pauseAction;
        playPauseButton.setAction(playAction);
        playPauseButton.setIcon(playIcon);
    }

    /**
     * Ustawia akcję dla przycisku stopu.
     *
     * @param action akcja dla przycisku stopu
     */
    public void setStopButtonAction(Action action) {
        stopButton.setAction(action);
        stopButton.setIcon(new ImageIcon(getImageFromResource("/player/stop.png")));
    }

    /**
     * Ustawia akcję dla przycisku 'następny'
     *
     * @param action akcja dla przycisku 'następny'
     */
    public void setNextButtonAction(Action action) {
        nextButton.setAction(action);
        nextButton.setIcon(new ImageIcon(getImageFromResource("/player/next.png")));
    }

    /**
     * Ustawia akcję dla przycisku 'poprzedni'
     *
     * @param action akcja dla przycisku 'poprzedni'
     */
    public void setPrevButtonAction(Action action) {
        prevButton.setAction(action);
        prevButton.setIcon(new ImageIcon(getImageFromResource("/player/prev.png")));
    }

    /**
     * Przełączenie przycisku pauzy/wznawiania.
     *
     * @param isPlaying wartość logiczna oznaczająca w jakim stanie ma znajdować się przycisk
     */
    public void switchPlayPauseButton(boolean isPlaying) {
        if (isPlaying) {
            playPauseButton.setAction(pauseAction);
            playPauseButton.setIcon(pauseIcon);
        } else {
            playPauseButton.setAction(playAction);
            playPauseButton.setIcon(playIcon);
        }
    }

    /**
     * Aktualizuje pozycję slidera pokazującego aktualny moment odtwarzania utworu.
     *
     * @param percentage wartość od 0 do 100
     */
    public void updateSongTime(int percentage) {
        slider.setValue(percentage);
    }

    /**
     * Aktualizuje wyświetlaną nazwę odtwarzanego pliku.
     *
     * @param name nazwa pliku
     */
    public void updateTrackNameLabel(String name) {
        trackNameText.setText(name);
    }
}
