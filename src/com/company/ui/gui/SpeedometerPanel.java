package com.company.ui.gui;

import com.company.logic.Dashboard;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Panel zawierający i rysujący prędkościomierz.
 */
class SpeedometerPanel extends JPanel {

    /**
     * Aktualna prędkość w km/h.
     */
    private float speed = 0;
    /**
     * Maksymalna prędkość w km/h.
     */
    private float maxSpeed = 200;
    /**
     * Aktualny bieg.
     */
    private char gear;

    /**
     * Określa kolor wartości na prędkościomierzu.
     */
    private Color digitColor = Color.ORANGE;
    /**
     * Rozmiar czcionki wartości na prędkościomierzu.
     */
    private int fontSize = 20;
    /**
     * Kolor wskaźnika prędkości.
     */
    private Color lineColor = Color.ORANGE;
    /**
     * Długość wskaźnika prędkości.
     */
    private int lineLength = 110;
    /**
     * Kolor symbolu biegu.
     */
    private Color gearColor = Color.WHITE;
    /**
     * Kolor tła prędkościomierza.
     */
    private Color bgColor = Color.BLACK;
    /**
     * Wartość logiczna określająca, czy rysować kropki przy wartościach prędkości.
     */
    private boolean ovals = true;
    /**
     * Wartość logiczna określająca stan aktywacji tempomatu.
     */
    private boolean isCruiseControlActive = false;

    /**
     * Inicjalizacja prędkościomierza - ustawienie Timera odświeżającego widok.
     */
    public SpeedometerPanel() {
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setLayout(new FlowLayout(FlowLayout.CENTER));

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                repaint();
            }
        }, 0, 10);
    }

    /**
     * Aktualizuje ustawienia graficzne prędkościomierza.
     *
     * @param guiSettings ustawienia graficzne
     */
    public void updateSettings(GUISettings guiSettings) {
        digitColor = new Color(guiSettings.getSetting("Kolor cyfr na tarczy zegara"));
        fontSize = guiSettings.getSetting("Wielkość czcionki na tarczy zegara");
        lineColor = new Color(guiSettings.getSetting("Kolor wskaźnika prędkości"));
        lineLength = guiSettings.getSetting("Długość wskaźnika prędkości");
        gearColor = new Color(guiSettings.getSetting("Kolor symbolu biegu"));
        bgColor = new Color(guiSettings.getSetting("Kolor tła zegara"));
        ovals = (guiSettings.getSetting("Rysowanie kropek") == 1);
    }

    /**
     * Aktualizuje prędkość, która ma być zwizualizowana.
     *
     * @param speed wartość prędkości w km/h
     */
    public void updateSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * Aktualizuje symbol aktualnego biegu.
     *
     * @param gear bieg
     */
    public void updateGear(Dashboard.Gear gear) {
        this.gear = gear.getAsChar();
    }

    /**
     * Ustawia prędkość maksymalną, która będzie na tarczy prędkościomierza i wpływa na jego wyskalowanie.
     *
     * @param maxSpeed wartość prędkości maksymalnej w km/h
     */
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    /**
     * Ustawia stan aktywacji tempomatu.
     *
     * @param cruiseControlActive stan aktywacji tempomatu
     */
    public void setCruiseControlActive(boolean cruiseControlActive) {
        isCruiseControlActive = cruiseControlActive;
    }

    @Override
    protected void paintComponent(Graphics g) {
        setBackground(bgColor);
        super.paintComponent(g);

        Point position = new Point(getPreferredSize().width / 2, getPreferredSize().height / 2);

        float minAngle = (float) (Math.PI / 4);
        float maxAngle = (float) (2 * Math.PI - Math.PI / 4);

        paintLabels(g, position, minAngle, maxAngle);
        if (isCruiseControlActive)
            paintCruiseControlIndicator(g, new Point(position.x - 50, position.y + 50));
        paintGear(g, new Point(position.x + 50, position.y + 50));
        paintPointer(g, position, minAngle, maxAngle);
    }

    /**
     * Rysuje wartości prędkości i, w zależności od ustawień, kropki przy nich.
     *
     * @param g        kontekst graficzny
     * @param position pozycja środka wokół, którego mają być rysowane wartości
     * @param minAngle minimalna wartość kąta, od którego mają być rysowane wartości
     * @param maxAngle maksymalna wartość kąta, do którego mają być rysowane wartości
     */
    private void paintLabels(Graphics g, Point position, float minAngle, float maxAngle) {
        int labelsPosRadius = 150;
        int dotsPosRadius = 120;
        int labelsCount = 10;
        float angleStep = (maxAngle - minAngle) / labelsCount;

        g.setFont(new Font("Century Gothic", Font.ITALIC, fontSize));
        g.setColor(digitColor);

        for (int i = 0; i <= labelsCount; i++) {
            float angle = minAngle + i * angleStep;
            int x = (int) ((float) (-Math.sin(angle)) * labelsPosRadius + position.x);
            int y = (int) ((float) (Math.cos(angle)) * labelsPosRadius + position.y);

            String str = Integer.toString(i * Math.round(maxSpeed / labelsCount));

            Rectangle2D stringBounds = g.getFontMetrics().getStringBounds(str, g);

            g.drawString(str, x - (int) (stringBounds.getWidth() / 2), y + (int) (stringBounds.getHeight() / 2));

            if (ovals) {
                x = (int) ((float) (-Math.sin(angle)) * dotsPosRadius + position.x);
                y = (int) ((float) (Math.cos(angle)) * dotsPosRadius + position.y);

                g.fillOval(x, y, 2, 2);
            }
        }
    }

    /**
     * Rysuje kontrolkę aktywacji tempomatu jako czerwoną kropkę.
     *
     * @param g        kontekst graficzny
     * @param position pozycja kontrolki
     */
    private void paintCruiseControlIndicator(Graphics g, Point position) {
        g.setColor(Color.RED);
        g.fillOval(position.x, position.y, 8, 8);
    }

    /**
     * Rysuje symbol biegu.
     *
     * @param g        kontekst graficzny
     * @param position pozycja
     */
    private void paintGear(Graphics g, Point position) {
        g.setFont(new Font("Century Gothic", Font.BOLD, 36));
        g.setColor(gearColor);
        g.drawString(String.valueOf(gear), position.x, position.y);
    }

    /**
     * Rysuje wskaźnik prędkościomierza.
     *
     * @param g        kontekst graficzny
     * @param position pozycja środka
     * @param minAngle minimalna wartość kąta położenia wskaźnika
     * @param maxAngle maksymalna wartość kąta położenia wskaźnika
     */
    private void paintPointer(Graphics g, Point position, float minAngle, float maxAngle) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2d.setColor(lineColor);

        float angle = minAngle + (maxAngle - minAngle) / maxSpeed * speed;
        int x = (int) ((float) (-Math.sin(angle)) * lineLength + position.x);
        int y = (int) ((float) (Math.cos(angle)) * lineLength + position.y);

        g2d.drawLine(position.x, position.y, x, y);
    }
}
