package com.company.ui.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Dodawanie i przechowywanie skrótów klawiaturowych wraz z ich akcjami.
 */
class KeyBindings {

    /**
     * Komponent, którego dotyczą skróty klawiszowe.
     */
    JComponent component;
    /**
     * Prefiks dla nazwy skrótów klawiszowych, które określają puszczenie przycisku.
     */
    final private String releasedKeyNamePrefix = "released";

    /**
     * Ustawia komponent dla którego ustawiane będą skróty klawiszowe.
     *
     * @param component komponent
     */
    public KeyBindings(JComponent component) {
        this.component = component;
    }

    /**
     * Dodaje skrót klawiszowy.
     *
     * @param description     opis skrótu klawiszowego
     * @param keyStrokeString skrót klawiszowy
     * @param action          akcja do przypisania danemu skrótowi
     */
    public void addKeyBinding(String description, String keyStrokeString, AbstractAction action) {
        component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(keyStrokeString), description);
        component.getActionMap().put(description, action);
    }

    /**
     * Dodaje skrót klawiszowy, który ma mieć różne akcje dla wciśnięcia oraz puszczenia.
     *
     * @param description     opis skrótu klawiszowego
     * @param keyStrokeString skrót klawiszowy
     * @param actionOnPress   akcja do przypisania danemu skrótowi przy wciśnięciu go
     * @param actionOnRelease akcja do przypisania danemu skrótowi przy puszczeniu go
     */
    public void addHoldKeyBinding(
            String description, String keyStrokeString, AbstractAction actionOnPress, AbstractAction actionOnRelease) {

        AbstractAction keyPressedAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnPress.actionPerformed(e);
                setEnabled(false);
            }
        };

        addKeyBinding(description + " (przytrzymanie)", keyStrokeString, keyPressedAction);

        addKeyBinding(releasedKeyNamePrefix + ' ' + description, releasedKeyNamePrefix + ' ' + keyStrokeString, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                actionOnRelease.actionPerformed(e);
                keyPressedAction.setEnabled(true);
            }
        });
    }

    /**
     * Zwraca zformatowany łańcuch znaków zawierający wszystkie skróty klawiszowe.
     *
     * @return łańcuch znaków ze skrótami klawiszowymi
     */
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();

        for (KeyStroke keyStroke : component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).keys()) {
            String name = (String) component.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).get(keyStroke);
            if (!name.startsWith(releasedKeyNamePrefix)) {
                out.append(keyStroke.toString().replace("pressed ", ""));
                out.append("\t");
                out.append(name);
                out.append('\n');
            }
        }

        return out.toString();
    }
}
