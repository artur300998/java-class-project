package com.company.ui.gui;

import javax.swing.*;

/**
 * Klasa reprezentująca okno z klawiszologią.
 */
public class KeyBindingsWindow extends JFrame {

    /**
     * Zawiera i wyświetla listę skrótów klawiszowych.
     */
    JTextArea keyBindingsTextArea;

    /**
     * Inicjalizuje okno i jego elementy.
     */
    public KeyBindingsWindow() {
        setSize(400, 300);
        setResizable(false);
        setTitle("Klawiszologia");
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        initComponents();
    }


    /**
     * Metoda wywoływana z konstruktora do inicjalizacji wewnętrznych komponentów okna.
     */
    private void initComponents() {
        keyBindingsTextArea = new JTextArea();
        keyBindingsTextArea.setEditable(false);
        getContentPane().add(keyBindingsTextArea);
    }

    /**
     * Wypełnia wnętrze okna podanym łańcuchem znaków.
     *
     * @param text łańcuch znaków, który ma zawierać okno
     */
    public void setText(String text) {
        keyBindingsTextArea.setText(text);
    }
}
