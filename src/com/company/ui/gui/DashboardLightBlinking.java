package com.company.ui.gui;

import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Reprezentacja kontrolki świateł, która miga po aktywowaniu.
 */
public class DashboardLightBlinking extends DashboardLight {

    /**
     * Timer za pomocą którego realizowane jest miganie kontrolki świateł.
     */
    private Timer blinkTimer;
    /**
     * Wartość logiczna oznaczająca stan wyświetlania obrazka w trybie migania.
     */
    private boolean blinkerIsLightUp = false;

    /**
     * Konstrukcja z użyciem klasy abstrakcyjnej {@link DashboardLight}.
     *
     * @param image obrazek, który będzie reprezentował stan kontrolki świateł
     */
    public DashboardLightBlinking(Image image) {
        super(image);
    }

    @Override
    void lightUp() {
        super.lightUp();

        blinkTimer = new Timer();
        blinkTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                blinkerIsLightUp = !blinkerIsLightUp;
                repaint();
            }
        }, 0, 500);
    }

    @Override
    void lightDown() {
        super.lightDown();

        blinkTimer.cancel();
        blinkerIsLightUp = false;
    }

    @Override
    public void switchLight() {
        super.switchLight();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (blinkerIsLightUp)
            g.drawImage(image, 0, 0, dimension.width, dimension.height, this);
    }
}
