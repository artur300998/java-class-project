package com.company.ui.gui;

import java.util.HashMap;

/**
 * Klasa reprezentująca ustawienia graficzne prędkościomierza przechowywane jako lista par (nazwa, wartość).
 */
public class GUISettings {

    /**
     * Ustawienia jako lista par (nazwa ustawienia, wartość).
     */
    private HashMap<String, Integer> settings;

    /**
     * Inicjalizacja mapy ustawień.
     */
    public GUISettings() {
        settings = new HashMap<>();
    }

    /**
     * Dodaje ustawienie.
     *
     * @param name  nazwa ustawienia
     * @param value wartość ustawienia
     */
    public void addSetting(String name, int value) {
        settings.put(name, value);
    }

    /**
     * Usuwa ustawienie o podanej nazwie.
     *
     * @param name nazwa ustawienia do usunięcia
     */
    public void removeSetting(String name) {
        settings.remove(name);
    }

    /**
     * Kopiuje ustawienia do tego obiektu.
     *
     * @param guiSettings ustawienia do skopiowania
     */
    public void setSettings(GUISettings guiSettings) {
        settings = new HashMap<>(guiSettings.settings);
    }

    /**
     * Zwraca wartość dla danego ustawienia.
     *
     * @param name nazwa ustawienia
     * @return wartość ustawienia
     */
    public int getSetting(String name) {
        return settings.get(name);
    }

    /**
     * Zwraca cały {@link HashMap} ze wszystkimi ustawieniami.
     *
     * @return lista par z ustawieniami
     */
    public HashMap<String, Integer> getAllSettings() {
        return settings;
    }
}
