package com.company.ui.gui;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Panel zawierający elementy GUI liczników.
 */
class CountersPanel extends JPanel {

    /**
     * Zawiera stan licznika.
     */
    private JLabel totalCounterValueLabel, userCounter1ValueLabel, userCounter2ValueLabel;
    /**
     * Przycisk zerujący licznik przebiegu użytkownika.
     */
    private JButton zeroUserCounter1Button, zeroUserCounter2Button;

    /**
     * Inicjalizacja panelu i jego komponentów.
     */
    public CountersPanel() {

        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createLineBorder(Color.BLACK));

        initComponents();
    }

    /**
     * Inicjalizacja komponentów panelu.
     */
    private void initComponents() {
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.ipadx = 2;
        c.ipady = 2;
        c.insets = new Insets(2, 2, 2, 2);
        c.fill = GridBagConstraints.BOTH;

        c.anchor = GridBagConstraints.LINE_START;
        add(new JLabel("Przebieg całkowity"), c);

        totalCounterValueLabel = new JLabel("0", SwingConstants.RIGHT);
        c.gridx++;
        add(totalCounterValueLabel, c);

        c.gridx++;
        add(new JLabel("km"), c);

        c.gridx++;
        add(new JLabel(""), c);

        c.gridy++;
        c.gridx = 0;
        add(new JLabel("Przebieg dzienny 1"), c);

        userCounter1ValueLabel = new JLabel("0", SwingConstants.RIGHT);
        userCounter1ValueLabel.setPreferredSize(new Dimension(70, userCounter1ValueLabel.getPreferredSize().height));
        c.gridx++;
        add(userCounter1ValueLabel, c);

        c.gridx++;
        add(new JLabel("km"), c);

        zeroUserCounter1Button = new JButton();
        c.gridx++;
        add(zeroUserCounter1Button, c);

        c.gridy++;
        c.gridx = 0;
        add(new JLabel("Przebieg dzienny 2"), c);

        userCounter2ValueLabel = new JLabel("0", SwingConstants.RIGHT);
        c.gridx++;
        add(userCounter2ValueLabel, c);

        c.gridx++;
        add(new JLabel("km"), c);

        zeroUserCounter2Button = new JButton("Wyzeruj");
        c.gridx++;
        add(zeroUserCounter2Button, c);
    }

    /**
     * Aktualizuje stan wyświetlanych liczników.
     *  @param totalCounter stan licznika przebiegu całkowitego
     * @param userCounter1 stan pierwszego licznika przebiegu użytkownika
     * @param userCounter2 stan drugiego licznika przebiegu użytkownika
     */
    public void updateCounters(int totalCounter, int userCounter1, int userCounter2) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        totalCounterValueLabel.setText(formatter.format(totalCounter / 1000.0f));
        userCounter1ValueLabel.setText(formatter.format(userCounter1 / 1000.0f));
        userCounter2ValueLabel.setText(formatter.format(userCounter2 / 1000.0f));
    }

    /**
     * Ustawia akcję dla przycisku 'Wyzeruj' obok pierwszego licznika użytkownika.
     *
     * @param action akcja dla przycisku
     */
    public void setZeroUserCounter1ButtonAction(Action action) {
        zeroUserCounter1Button.setAction(action);
        zeroUserCounter1Button.setText("Wyzeruj");
    }

    /**
     * Ustawia akcję dla przycisku 'Wyzeruj' obok drugiego licznika użytkownika.
     *
     * @param action akcja dla przycisku
     */
    public void setZeroUserCounter2ButtonAction(Action action) {
        zeroUserCounter2Button.setAction(action);
        zeroUserCounter2Button.setText("Wyzeruj");
    }
}
