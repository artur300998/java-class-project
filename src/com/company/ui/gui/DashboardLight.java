package com.company.ui.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Reprezentacja kontrolki świateł.
 */
abstract class DashboardLight extends JPanel {

    /**
     * Obrazek wyświetlany podczas aktywacji danej kontrolki świateł.
     */
    protected Image image;
    /**
     * Wartość logiczna oznaczająca stan aktywacji kontrolki świateł.
     */
    protected boolean isLightUp = false;
    /**
     * Wymiary w jakich ma być wyświetlany obrazek.
     */
    protected Dimension dimension;

    /**
     * Tworzy kontrolkę świateł z podaną grafiką.
     *
     * @param image obrazek, który będzie reprezentował stan kontrolki świateł
     */
    public DashboardLight(Image image) {
        this.image = image;

        float scale = 0.5f;
        dimension = new Dimension(Math.round(this.image.getWidth(this) * scale), Math.round(this.image.getHeight(this) * scale));

        setPreferredSize(dimension);

        setBackground(Color.BLACK);
    }

    /**
     * Zapalenie kontrolki.
     */
    void lightUp() {
        isLightUp = true;
        repaint();
    }

    /**
     * Zgaszenie kontrolki.
     */
    void lightDown() {
        isLightUp = false;
        repaint();
    }

    /**
     * Przełączenie kontrolki.
     */
    public void switchLight() {
        if (isLightUp)
            lightDown();
        else
            lightUp();

        repaint();
    }

    /**
     * Zwraca stan aktywacji kontrolki.
     *
     * @return stan aktywacji kontrolki
     */
    public boolean isLightUp() {
        return isLightUp;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
    }
}
