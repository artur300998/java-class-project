package com.company.ui.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Klasa reprezentująca okno ustawień.
 */
class SettingsWindow extends JFrame {

    /**
     * Tabela, która wyświetla ustawienia i pozwala na ich zmianę.
     */
    JTable table;
    /**
     * Opcja ustawień, która określa, czy rysować kropki przy wartościach na prędkościomierzu.
     */
    JCheckBox dotsDrawingCheckbox;
    /**
     * Przycisk 'Zastosuj', który stosuje ustawienia w takim stanie, w jakim są w tabeli.
     */
    JButton applyButton;

    /**
     * Inicjalizuje okno i jego elementy.
     */
    public SettingsWindow() {
        setSize(400, 200);
        setResizable(false);
        setTitle("Ustawienia");
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        initComponents();
    }

    /**
     * Metoda wywoływana z konstruktora do inicjalizacji wewnętrznych komponentów okna.
     */
    private void initComponents() {

        JPanel settingsPanel = new JPanel();
        settingsPanel.setLayout(new BoxLayout(settingsPanel, BoxLayout.Y_AXIS));
        TableCellRenderer coloredRenderer = new DefaultTableCellRenderer() {
            @Override
            public Component getTableCellRendererComponent(
                    JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

                Component cellComponent = super.getTableCellRendererComponent(
                        table, value, isSelected, hasFocus, row, column);
                if (column == 1 && (row != 1 && row != 3)) {
                    int colorNum = Integer.parseInt((String) table.getModel().getValueAt(row, column));
                    Color newColor = new Color(colorNum);
                    cellComponent.setBackground(newColor);
                    setValue("");
                } else {
                    cellComponent.setBackground(Color.WHITE);
                    cellComponent.setForeground(Color.BLACK);
                    setValue((String) table.getModel().getValueAt(row, column));
                }

                return cellComponent;
            }
        };

        table = new JTable(6, 2) {
            public boolean isCellEditable(int row, int column) {
                if (column == 0)
                    return false;
                else
                    return true;
            }

            public TableCellRenderer getCellRenderer(int row, int column) {
                return coloredRenderer;
            }
        };

        table.setValueAt("Kolor cyfr na tarczy zegara", 0, 0);
        table.setValueAt("Wielkość czcionki na tarczy zegara", 1, 0);
        table.setValueAt("Kolor wskaźnika prędkości", 2, 0);
        table.setValueAt("Długość wskaźnika prędkości", 3, 0);
        table.setValueAt("Kolor symbolu biegu", 4, 0);
        table.setValueAt("Kolor tła zegara", 5, 0);

        table.setValueAt(Integer.toString(Color.ORANGE.getRGB()), 0, 1);
        table.setValueAt("20", 1, 1);
        table.setValueAt(Integer.toString(Color.ORANGE.getRGB()), 2, 1);
        table.setValueAt("110", 3, 1);
        table.setValueAt(Integer.toString(Color.WHITE.getRGB()), 4, 1);
        table.setValueAt(Integer.toString(Color.BLACK.getRGB()), 5, 1);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable) e.getSource();
                int row = target.getSelectedRow();
                int column = target.getSelectedColumn();
                if (column == 1 && (row != 1 && row != 3)) {
                    Color newColor = JColorChooser.showDialog(null, "Change color", Color.WHITE);
                    if (newColor != null)
                        table.setValueAt(Integer.toString(newColor.getRGB()), row, column);
                }
            }
        });

        dotsDrawingCheckbox = new JCheckBox("Rysowanie kropek");
        dotsDrawingCheckbox.setAlignmentX(Component.LEFT_ALIGNMENT);

        settingsPanel.add(table);
        settingsPanel.add(dotsDrawingCheckbox);

        applyButton = new JButton("Zastosuj");

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(applyButton);

        getContentPane().add(settingsPanel);
        getContentPane().add(buttonsPanel);
    }

    /**
     * Przekazuje dane o ustawieniach do komponentu tabeli.
     *
     * @param settings ustawienia do przekazania
     */
    public void setGuiSettings(GUISettings settings) {
        for (int i = 0; i < 6; i++) {
            table.setValueAt(Integer.toString(settings.getSetting((String) table.getValueAt(i, 0))), i, 1);
        }
        dotsDrawingCheckbox.setSelected(settings.getSetting(dotsDrawingCheckbox.getText()) == 1);
    }

    /**
     * Zwraca ustawienia znajdujące się w tabeli.
     *
     * @return ustawienia znajdujące się w tabeli
     */
    public GUISettings getGuiSettings() {
        GUISettings guiSettings = new GUISettings();

        for (int i = 0; i < table.getModel().getRowCount(); i++)
            if (table.getModel().getValueAt(i, 1) != null)
                guiSettings.addSetting((String) table.getModel().getValueAt(i, 0),
                        Integer.parseInt((String) table.getModel().getValueAt(i, 1)));

        guiSettings.addSetting(dotsDrawingCheckbox.getText(), boolToInt(dotsDrawingCheckbox.isSelected()));

        return guiSettings;
    }

    /**
     * Ustawia akcję dla przycisku "Zastosuj".
     *
     * @param action akcja, która ma zostać wykonana dla przycisku
     */
    public void setApplyButtonAction(Action action) {
        applyButton.setAction(action);
        applyButton.setText("Zastosuj");
    }

    /**
     * Zamienia wartość logiczną na typ prymitywny {@link int}.
     *
     * @param b wartość logiczna, która ma zostać przekonwertowana
     * @return wartość całkowita odpowiadająca podanej wartości logicznej
     */
    private int boolToInt(boolean b) {
        return b ? 1 : 0;
    }
}
