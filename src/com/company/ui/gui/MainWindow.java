package com.company.ui.gui;

import com.company.logic.MainLogic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * Klasa reprezentujące główne okno aplikacji.
 * Zawiera menu i główny panel deski rozdzielczej.
 */
public class MainWindow extends JFrame implements ActionListener {

    /**
     * Pasek menu.
     */
    private JMenuBar menuBar;
    /**
     * Kategoria menu.
     */
    private JMenu mainMenu, helpMenu;
    /**
     * Podmenu menu 'Plik'.
     */
    private JMenu submenuOpen, submenuSave;
    /**
     * Element menu.
     */
    private JMenuItem menuItemOpenFromXML, menuItemOpenFromJDBC,
            menuItemSaveToXML, menuItemSaveToJDBC,
            menuItemSettings, menuItemExit, menuItemKeyBindings, menuItemAbout;

    /**
     * Okno ustawień.
     */
    private SettingsWindow settingsWindow;
    /**
     * Okno klawiszologii.
     */
    private KeyBindingsWindow keyBindingsWindow;

    /**
     * Panel deski rozdzielczej.
     */
    private DashboardPanel dashboardPanel;

    /**
     * Kontroler GUI.
     */
    private GUIController guiController;

    /**
     * Inicjalizuje okno i jego elementy.
     */
    public MainWindow() {

        Container container = getContentPane();
        setSize(800, 600);
        setResizable(false);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setTitle("Programowanie komponentowe - PROJEKT");
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));

        initComponents();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                guiController.exitTasks();
                MainLogic.getInstance().getDashboard().stop();
                System.exit(0);
            }
        });

        dashboardPanel.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK), "Alt+F4");
        dashboardPanel.getActionMap().put("Alt+F4", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exit();
            }
        });

        setVisible(true);

        new Thread(MainLogic.getInstance()).start();

        guiController = new GUIController(this);
    }

    /**
     * Metoda wywoływana z konstruktora do inicjalizacji wewnętrznych komponentów okna.
     */
    private void initComponents() {

        settingsWindow = new SettingsWindow();
        keyBindingsWindow = new KeyBindingsWindow();

        menuBar = new JMenuBar();

        mainMenu = new JMenu("Plik");

        submenuOpen = new JMenu("Otwórz");
        menuItemOpenFromXML = new JMenuItem("z XML...");
        menuItemOpenFromJDBC = new JMenuItem("z JDBC...");
        submenuOpen.add(menuItemOpenFromXML);
        submenuOpen.add(menuItemOpenFromJDBC);

        submenuSave = new JMenu("Zapisz");
        menuItemSaveToXML = new JMenuItem("do XML...");
        menuItemSaveToJDBC = new JMenuItem("do JDBC...");
        submenuSave.add(menuItemSaveToXML);
        submenuSave.add(menuItemSaveToJDBC);

        menuItemSettings = new JMenuItem("Ustawienia");
        menuItemExit = new JMenuItem("Wyjście");

        submenuSave.addActionListener(this);
        menuItemOpenFromXML.addActionListener(this);
        menuItemOpenFromJDBC.addActionListener(this);
        submenuOpen.addActionListener(this);
        menuItemSaveToXML.addActionListener(this);
        menuItemSaveToJDBC.addActionListener(this);
        menuItemSettings.addActionListener(this);
        menuItemExit.addActionListener(this);

        mainMenu.add(submenuOpen);
        mainMenu.add(submenuSave);
        mainMenu.add(menuItemSettings);
        mainMenu.add(menuItemExit);

        helpMenu = new JMenu("Pomoc");

        menuItemKeyBindings = new JMenuItem("Klawiszologia");
        menuItemKeyBindings.addActionListener(this);
        helpMenu.add(menuItemKeyBindings);

        menuItemAbout = new JMenuItem("O programie");
        menuItemAbout.addActionListener(this);
        helpMenu.add(menuItemAbout);

        menuBar.add(mainMenu);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);

        dashboardPanel = new DashboardPanel();

        getContentPane().add(dashboardPanel);

        dashboardPanel.setVisible(true);
    }

    /**
     * Wywoływane przy wyjściu z aplikacji.
     * Wysyła komunikat o zamykaniu się okna.
     */
    private void exit() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source instanceof JMenuItem) {
            if (source == menuItemOpenFromXML) {
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showOpenDialog(this);
                if (returnVal == JFileChooser.APPROVE_OPTION)
                    guiController.readFromXML(fc.getSelectedFile().getAbsolutePath());
            } else if (source == menuItemOpenFromJDBC) {
                guiController.readFromDB();
            } else if (source == menuItemSaveToXML) {
                JFileChooser fc = new JFileChooser();
                int returnVal = fc.showSaveDialog(this);
                if (returnVal == JFileChooser.APPROVE_OPTION)
                    guiController.saveToXML(fc.getSelectedFile().getAbsolutePath());
            } else if (source == menuItemSaveToJDBC) {
                guiController.saveToDB();
            } else if (source == menuItemSettings) {
                guiController.showSettingsWindow();
            } else if (source == menuItemExit) {
                exit();
            } else if (source == menuItemKeyBindings) {
                guiController.showKeyBindingsWindow();
            } else if (source == menuItemAbout) {
                JOptionPane.showMessageDialog(this, "Projekt na przedmiot programowanie komponentowe.\nArtur Nowicki\nKevin Cuadra\nPolitechnika Łódzka");
            }
        }
    }

    /**
     * Zwraca okno ustawień.
     *
     * @return okno ustawień
     */
    public SettingsWindow getSettingsWindow() {
        return settingsWindow;
    }

    /**
     * Zwraca okno z klawiszologią.
     *
     * @return okno z klawiszologią
     */
    public KeyBindingsWindow getKeyBindingsWindow() {
        return keyBindingsWindow;
    }

    /**
     * Zwraca główny panel okna.
     *
     * @return główny panel okna
     */
    public DashboardPanel getDashboardPanel() {
        return dashboardPanel;
    }
}
