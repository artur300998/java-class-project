package com.company.ui.gui;

import com.company.logic.Lights;

import javax.swing.*;
import java.awt.*;

import static com.company.data.Resources.getImageFromResource;

/**
 * Panel zawierający kontrolki świateł.
 */
class DashboardLightsPanel extends JPanel {

    /**
     * Kontrolka świateł, która świeci ciągle po aktywacji.
     */
    private DashboardLightConstant lowBeamHeadlight, highBeamHeadlight, positionLights, fogLightsFront, fogLightsBack;
    /**
     * Kontrolka świateł, która miga po aktywacji.
     */
    private DashboardLightBlinking leftIndicator, rightIndicator;

    /**
     * Inicjalizacja panelu i jego komponentów.
     */
    public DashboardLightsPanel() {

        setLayout(new FlowLayout(FlowLayout.CENTER));
        setBorder(BorderFactory.createLineBorder(Color.BLACK));
        setBackground(Color.BLACK);

        initComponents();
    }

    /**
     * Inicjalizacja komponentów panelu.
     */
    private void initComponents() {
        lowBeamHeadlight = new DashboardLightConstant(getImageFromResource("/lights/mijania.png"));
        highBeamHeadlight = new DashboardLightConstant(getImageFromResource("/lights/drogowe.png"));
        positionLights = new DashboardLightConstant(getImageFromResource("/lights/pozycyjne.png"));
        fogLightsFront = new DashboardLightConstant(getImageFromResource("/lights/przeciwmglowe_przednie.png"));
        fogLightsBack = new DashboardLightConstant(getImageFromResource("/lights/przeciwmglowe_tylne.png"));
        leftIndicator = new DashboardLightBlinking(getImageFromResource("/lights/k_lewy.png"));
        rightIndicator = new DashboardLightBlinking(getImageFromResource("/lights/k_prawy.png"));

        add(leftIndicator);
        add(lowBeamHeadlight);
        add(highBeamHeadlight);
        add(positionLights);
        add(fogLightsFront);
        add(fogLightsBack);
        add(rightIndicator);

        leftIndicator.setVisible(true);
        rightIndicator.setVisible(true);
        lowBeamHeadlight.setVisible(true);
        highBeamHeadlight.setVisible(true);
        positionLights.setVisible(true);
        fogLightsFront.setVisible(true);
        fogLightsBack.setVisible(true);
    }

    /**
     * Aktualizuje stan kontrolek.
     *
     * @param lights obiekt zawierający stany aktywacji świateł
     */
    public void updateLightsState(Lights lights) {
        if (lowBeamHeadlight.isLightUp() != lights.getLightState(Lights.LightType.LOWBEAM_HEADLIGHT))
            lowBeamHeadlight.switchLight();
        if (highBeamHeadlight.isLightUp() != lights.getLightState(Lights.LightType.HIGHBEAM_HEADLIGHT))
            highBeamHeadlight.switchLight();
        if (positionLights.isLightUp() != lights.getLightState(Lights.LightType.POSITION_LIGHT))
            positionLights.switchLight();
        if (fogLightsFront.isLightUp() != lights.getLightState(Lights.LightType.FOG_FRONT))
            fogLightsFront.switchLight();
        if (fogLightsBack.isLightUp() != lights.getLightState(Lights.LightType.FOG_BACK))
            fogLightsBack.switchLight();
        if (leftIndicator.isLightUp() != lights.getLightState(Lights.LightType.LEFT_INDICATOR))
            leftIndicator.switchLight();
        if (rightIndicator.isLightUp() != lights.getLightState(Lights.LightType.RIGHT_INDICATOR))
            rightIndicator.switchLight();
    }
}
