package com.company.ui.console;

import com.company.data.Resources;
import com.company.data.inout.ConfigManagerException;
import com.company.logic.Dashboard;
import com.company.logic.Lights;
import com.company.logic.MainLogic;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Klasa obsługuje polecenia na podstawie znaków podanych przez użytkownika
 */
public class ConsoleController {

    /**
     * Deska rozdzielcza
     */
    private Dashboard dashboard;

    /**
     * Inicjalizuje i uruchamia klasę dashboard
     */
    public ConsoleController()
    {
        dashboard = MainLogic.getInstance().getDashboard();
        new Thread() {
            @Override
            public void run() {
                dashboard.run();
            }

        }.start();
        dashboard.switchCruiseControlActivation();
        readFromXML(Resources.getURIToResource(".").resolve("config.xml").getPath());
    }

    /**
     * Wykonuje polecenie na podstawie podanego znaku
     *
     * @param ch znak podany przez użytkownika
     */
    public void playAction(char ch)
    {
        switch(ch)
        {
            case 'h':
            case 'H':
                showHelpScreen();
                break;
            case 'a':
            case 'A':
                increaseSpeed();
                break;
            case 'd':
            case 'D':
                decreaseSpeed();
                break;
            case 's':
            case 'S':
                showCurrentSpeed();
                break;
            case 'k':
            case 'K':
                switchOffLights();
                break;
            case 'l':
            case 'L':
                switchOnLights();
                break;
            case 'n':
            case 'N':
                switchFrontLights();
                break;
            case 'm':
            case 'M':
                switchBackLights();
                break;
            case 'j':
            case 'J':
                showLights();
                break;
            case 'c':
            case 'C':
                showCounters();
                break;
            case '1':
                resetCounter1();
                break;
            case '2':
                resetCounter2();
                break;
            case 'b':
            case 'B':
                showOnBoardComputerData();
                break;
            case 'q':
            case 'Q':
                dashboard.stop();
                exitTasks();
                break;
            default:
                System.out.println("Nie znaleziono polecenia");
                break;
        }
    }

    /**
     * Pokazuje dane z kopmutera pokładowego
     */
    private void showOnBoardComputerData() {
        System.out.println(dashboard.getOnBoardComputerData().toString());
    }

    /**
     * Zeruje drugi licznik przebiegu
     */
    private void resetCounter2() {
        dashboard.getDataRepository().resetUserCounter2();
    }

    /**
     * Zeruje peirwszy licznik przebiegu
     */
    private void resetCounter1() {
        dashboard.getDataRepository().resetUserCounter1();
    }

    /**
     * Pokazuje liczniki przebiegów
     */
    private void showCounters() {
        String counters = "";
        NumberFormat formatter = new DecimalFormat("#0.00");

        counters += "Przebieg całkowity: " + formatter.format(dashboard.getDataRepository().getTotalCounterValue()/1000.0) + " km";
        counters += "\nPrzebieg dzienny 1: " + formatter.format(dashboard.getDataRepository().getUserCounter1Value()/1000.0) + " km";
        counters += "\nPrzebieg dzienny 2: " + formatter.format(dashboard.getDataRepository().getUserCounter2Value()/1000.0) + " km";

        System.out.println(counters);
    }

    /**
     * Pokazuje stan świateł
     */
    private void showLights() {
        String lights = "Zapalone światła: ";
        if(dashboard.getLights().getLightState(Lights.LightType.POSITION_LIGHT))
            lights += "pozycyjne, ";
        if(dashboard.getLights().getLightState(Lights.LightType.LOWBEAM_HEADLIGHT))
            lights += "mijania, ";
        if(dashboard.getLights().getLightState(Lights.LightType.HIGHBEAM_HEADLIGHT))
            lights += "drogowe, ";
        if(dashboard.getLights().getLightState(Lights.LightType.FOG_FRONT))
            lights += "przeciwmgielne przednie, ";
        if(dashboard.getLights().getLightState(Lights.LightType.FOG_BACK))
            lights += "przeciwmgielne tylne, ";
        if(lights == "Zapalone światła: ")
            lights += "żadne";
        else
            lights = lights.substring(0, lights.length() - 2);

        System.out.println(lights);
    }

    /**
     * Przełącza światła przeciwmgielne tylne
     */
    private void switchBackLights() {
        dashboard.getLights().switchLightState(Lights.LightType.FOG_BACK);
    }

    /**
     * Przełącza światła przeciwmgielne przednie
     */
    private void switchFrontLights() {
        dashboard.getLights().switchLightState(Lights.LightType.FOG_FRONT);
    }

    /**
     * Wyłącza światła (drogowe, mijania, pozycyjne)
     */
    private void switchOffLights() {
        dashboard.getLights().cycleHeadlightsState(false);

    }

    /**
     * Włącza światła (pozycyjne, mijania, drogowe)
     */
    private void switchOnLights() {
        dashboard.getLights().cycleHeadlightsState(true);
    }

    /**
     * Pokazuje obecną prędkość
     */
    private void showCurrentSpeed() {
        String speed = Float.toString(dashboard.getSpeed());

        System.out.println(speed + " km/h");
    }

    /**
     * Pokazuje klawisze z opisem polecenia
     */
    private void showHelpScreen()
    {
        String helpScreen = "Okno pomocy" +
                "\nPoniżej podane są opisy poszczególnych klawiszy" +
                "\n[h] - wyświetla to okno" +
                "\n[a] - zwieksza predkosc o 5" +
                "\n[d] - zmniejsza predkosc o 5" +
                "\n[s] - pokazuje obecną prędkość" +
                "\n[k] - wyłącza kolejno światła drogowe/mijania/postojowe" +
                "\n[l] - włącza kolejno światła postojowe/mijania/drogowe" +
                "\n[n] - włącza/wyłącza światła przeciwmgielne przednie" +
                "\n[m] - włącza/wyłącza światła przeciwmgielne tylne" +
                "\n[j] - pokazuje stan świateł" +
                "\n[c] - prezentuje przebiegi" +
                "\n[1] - zeruje pierwszy przebieg" +
                "\n[2] - zeruje drugi przebieg" +
                "\n[b] - prezentuje dane z komputera" +
                "\n[q] - kończy działanie programu";

        System.out.println(helpScreen);
    }

    /**
     * Zwiększa prędkość o 5 km/h
     */
    private void increaseSpeed()
    {
        dashboard.cruiseControlAddSpeed();

        System.out.println("Prędkość zwiększona");
    }

    /**
     * Zmniejsza prędkość o 5 km/h
     */
    private void decreaseSpeed()
    {
        dashboard.cruiseControlRemoveSpeed();

        System.out.println("Prędkość zmniejszona");
    }

    /**
     * Wywołuje zapis stanu aplikacji do pliku XML.
     *
     * @param filePath ścieżka do pliku XML
     */
    public void saveToXML(String filePath) {
        try {
            dashboard.getDataRepository().saveConfigurationToFile(filePath);
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Wywołuje odczyt stanu aplikacji z pliku XML.
     *
     * @param filePath ścieżka do pliku XML
     */
    public void readFromXML(String filePath) {
        try {
            dashboard.getDataRepository().readConfigurationFromFile(filePath);
        } catch (ConfigManagerException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Zapisuje stan aplikacji do pliku XML w domyślnej ścieżce przy wychodzeniu aplikacji.
     */
    public void exitTasks() {
        saveToXML(Resources.getURIToResource(".").resolve("config.xml").getPath());
    }
}
