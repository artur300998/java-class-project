package com.company.ui.console;

import java.util.Scanner;

/**
 * Klasa reprezentująca interfejs w trybie tekstowym
 */
public class BasicUI {

    /**
     * Skaner do wczytywania znaków
     */
     private Scanner scan;


    /**
     * Kontroler konsoli
     */
     private ConsoleController consoleController;

     /**
      * Wyświetla okno powitalne, inicjalizuje pola i wywołuje pętlę
      */
     public BasicUI()
     {
         String welcomeScreen = "Programowanie komponentowe - PROJEKT" +
                 "\n\nDeska rozdzielcza" +
                 "\n****************" +
                 "\n[h] - pomoc";
         System.out.println(welcomeScreen);
         scan = new Scanner(System.in);
         consoleController = new ConsoleController();
         waitForInput();
     }

     /**
      * Pętla z wczytywaniem znaków od użytkownika
      */
     private void waitForInput()
     {
          while(true)
          {
               char ch = scan.nextLine().charAt(0);

               consoleController.playAction(ch);
               if(ch == 'q' || ch == 'Q')
                    break;
          }
     }
}
