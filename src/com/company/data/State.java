package com.company.data;

/**
 * Przechowuje właściwości, które są zapisywane w historii podróży.
 */
public class State {
    /**
     * Prędkość.
     */
    public Float speed;
    /**
     * Licznik przebiegu w km.
     */
    public Integer totalCounter, userCounter1, userCounter2;
    /**
     * Średnia prędkość.
     */
    public Float avgSpeed;
    /**
     * Maksymalna prędkość.
     */
    public Float maxSpeed;
    /**
     * Czas podróży w min.
     */
    public Integer tripTime;
    /**
     * Przebyty dystans w km.
     */
    public Float distance;
    /**
     * Średnie spalanie w l/100km.
     */
    public Float avgCombustion;
}
