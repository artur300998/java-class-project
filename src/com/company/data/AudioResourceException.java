package com.company.data;

/**
 * Wyrzucony, gdy wystąpił błąd podczas obsługi strumieni audio.
 */
public class AudioResourceException extends Exception {

    /**
     * Konstruuje {@link AudioResourceException} ze szczegółowym komunikatem.
     *
     * @param message szczegółowy komunikat
     */
    public AudioResourceException(String message) {
        super(message);
    }
}
