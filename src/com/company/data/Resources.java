package com.company.data;

import com.company.Main;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;

/**
 * Pomocnicza klasa do pozyskiwania zasobów.
 */
public class Resources {

    /**
     * Zwraca obiekt {@link Image} z odczytanym obrazem.
     *
     * @param path ścieżka względna do pliku obrazu
     * @return obiekt obrazu
     */
    static public Image getImageFromResource(String path) {
        Image image;
        try {
            image = ImageIO.read(Main.class.getResourceAsStream(path));
            return image;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
    }

    /**
     * Zwraca łańcuch znaków potrzebny do połączenia się z bazą danych MS SQL z danymi odczytanymi z pliku.
     * Plik musi posiadać zawartość zgodną z tym czego oczekuje klasa {@link Properties} i atrybuty:
     * "db.server", "db.databaseName", "db.username", "db.password".
     *
     * @param path ścieżka względna do pliku konfiguracyjnego
     * @return łańcuch znaków zawierający dane do połączenia z pliku
     */
    static public String getJDBCConnectionStringFromFile(String path) {
        String connectionStr = null;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(getURIToResource(path).getPath()));
            connectionStr =
                    "jdbc:sqlserver://" + properties.getProperty("db.server")       + ';'
                            + "databaseName="   + properties.getProperty("db.databaseName") + ';'
                            + "user="           + properties.getProperty("db.username")     + ';'
                            + "password="       + properties.getProperty("db.password")     + ';';
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connectionStr;
    }

    /**
     * Zwraca {@link URI} do pliku.
     *
     * @param resourceRelativePath ścieżka względna do pliku
     * @return obiekt {@link URI} odnoszący się do pliku podanego w argumencie
     */
    static public URI getURIToResource(String resourceRelativePath) {
        URI uri = null;
        try {
            uri = Resources.class.getClassLoader().getResource(resourceRelativePath).toURI();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uri;
    }
}
