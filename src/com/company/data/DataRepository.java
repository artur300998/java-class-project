package com.company.data;

import com.company.data.inout.*;
import com.company.ui.gui.GUISettings;

/**
 * Klasa przechowująca aktualne dane, które mogą być zapisywane (konfiguracja, przebieg podróży).
 */
public class DataRepository {

    /**
     * Aktualna konfiguracja.
     */
    private Config config;

    /**
     * Menedżer udostępniający funkcjonalność zapisu przebiegu podróży.
     */
    private JDBCLogManager jdbcLogManager;

    /**
     * Tworzy domyślną konfigurację wewnątrz klasy.
     */
    public DataRepository() {
        config = new Config();
        try {
            jdbcLogManager = new JDBCLogManager();
        } catch (JDBCManagerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Zapisuje aktualny stan konfiguracji do pliku XML.
     *
     * @param filePath ścieżka do pliku, do którego ma być zapisany stan
     * @throws ConfigManagerException jeżeli zapis się nie powiedzie
     */
    public void saveConfigurationToFile(String filePath) throws ConfigManagerException {
        XMLConfigManager xmlConfigManager = new XMLConfigManager(filePath);
        xmlConfigManager.registerAliases(config);
        xmlConfigManager.saveConfig(config);
    }

    /**
     * Wczytuje konfigurację z pliku i ustawia je jako aktualną konfigurację.
     *
     * @param filePath ścieżka do pliku, z którego mają być wczytane dane
     * @throws ConfigManagerException jeżeli wczytanie z pliku nie powiedzie się
     */
    public void readConfigurationFromFile(String filePath) throws ConfigManagerException {
        XMLConfigManager xmlConfigManager = new XMLConfigManager(filePath);
        xmlConfigManager.registerAliases(config);
        config = xmlConfigManager.getConfig();
    }

    /**
     * Zapisuje aktualny stan konfiguracji do bazy danych.
     *
     * @throws ConfigManagerException jeżeli zapis do bazy danych nie powiedzie się
     */
    public void saveConfigurationToDatabase() throws ConfigManagerException {
        JDBCConfigManager jdbcConfigManager = new JDBCConfigManager();
        jdbcConfigManager.saveConfig(config);
    }

    /**
     * Wczytuje konfigurację z bazy danych i ustawia je jako aktualną konfigurację.
     *
     * @throws ConfigManagerException jeżeli wczytanie z bazy danych nie powiedzie się
     */
    public void readConfigurationFromDatabase() throws ConfigManagerException {
        JDBCConfigManager jdbcConfigManager = new JDBCConfigManager();
        config = jdbcConfigManager.getConfig();
    }

    /**
     * Zapisuje stan podróży.
     *
     * @param state stan podróży
     */
    public void saveStateToDatabaseLog(State state) {
        try {
            jdbcLogManager.logState(state);
        } catch (JDBCManagerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Wykonanuje operacje mające na celu zwolnienie zasobów, zamknięcie połączeń itd.
     */
    public void closeRepository() {
        jdbcLogManager.closeManager();
    }

    /**
     * Dodanie podanej wartości do licznika przebiegu całkowitego.
     *
     * @param val wartość, która ma zostać dodana
     * @throws CounterException jeżeli wartość licznika po dodaniu byłaby nie prawidłowa (przepełnieie)
     */
    public void addToTotalCounter(float val) throws CounterException {
        config.getTotalCounter().add(val);
    }

    /**
     * Dodanie podanej wartości do pierwszego licznika przebiegu użytkownika.
     *
     * @param val wartość, która ma zostać dodana
     * @throws CounterException jeżeli wartość licznika po dodaniu byłaby nie prawidłowa (przepełnieie)
     */
    public void addToUserCounter1(float val) throws CounterException {
        config.getUserCounter1().add(val);
    }

    /**
     * Dodanie podanej wartości do drugiego licznika przebiegu użytkownika.
     *
     * @param val wartość, która ma zostać dodana
     * @throws CounterException jeżeli wartość licznika po dodaniu byłaby nie prawidłowa (przepełnieie)
     */
    public void addToUserCounter2(float val) throws CounterException {
        config.getUserCounter2().add(val);
    }

    /**
     * Resetuje pierwszy licznik przebiegu użytkownika.
     */
    public void resetUserCounter1() {
        config.getUserCounter1().reset();
    }

    /**
     * Resetuje drugi licznik przebiegu użytkownika.
     */
    public void resetUserCounter2() {
        config.getUserCounter2().reset();
    }

    /**
     * Ustawia aktualne ustawienia.
     *
     * @param settings aktualne ustawienia
     */
    public void setSettings(GUISettings settings) {
        config.getGuiSettings().setSettings(settings);
    }

    /**
     * Zwraca ustawienia.
     *
     * @return ustawienia
     */
    public GUISettings getGuiSettings() {
        return config.getGuiSettings();
    }

    /**
     * Zwraca wartość licznika przebiegu całkowitego.
     *
     * @return wartość licznika przebiegu całkowitego
     */
    public int getTotalCounterValue() {
        return config.getTotalCounter().getValue();
    }

    /**
     * Zwraca wartość pierwszego licznika użytkownika.
     *
     * @return wartość pierwszego licznika użytkownika
     */
    public int getUserCounter1Value() {
        return config.getUserCounter1().getValue();
    }

    /**
     * Zwraca wartość drugiego licznika użytkownika.
     *
     * @return wartość drugiego licznika użytkownika
     */
    public int getUserCounter2Value() {
        return config.getUserCounter2().getValue();
    }

}
