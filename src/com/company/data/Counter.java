package com.company.data;

import java.io.Serializable;

/**
 * Klasa reprezentująca licznik.
 */
public class Counter implements Serializable, Comparable<Counter> {

    /**
     * Wartość licznika.
     */
    private float counter;

    /**
     * Inicjalizacja licznika zerem.
     */
    public Counter() {
        this.counter = 0;
    }

    /**
     * Inicjalizacja licznika wartością podaną w argumencie wraz ze sprawdzenie poprawności.
     *
     * @param value wartość, którą ma być zainicjowany licznik
     * @throws CounterException jeżeli wartość jest nie prawidłowa (mniejsza od zera)
     */
    public Counter(int value) throws CounterException {
        if (value < 0)
            throw new CounterException("Counter less than zero on initialization.");
        this.counter = value;
    }

    /**
     * Zwraca wartość licznika.
     *
     * @return wartość licznika
     */
    public int getValue() {
        return (int) counter;
    }

    /**
     * Sprawdza poprawność przechowywanego wewnątrz licznika.
     *
     * @throws CounterException jeżeli przechowywana wartość jest nie poprawna (mniejsza od zera)
     */
    public void validateCounter() throws CounterException {
        if (counter < 0)
            throw new CounterException("Counter less than zero on validation.");
    }

    /**
     * Dodaje wartość podaną w argumencie do licznika.
     *
     * @param value wartość, która ma zostać dodana do licznika
     * @throws CounterException jeżeli wartość powstała po dodaniu jest nie poprawna (overflow)
     */
    public void add(float value) throws CounterException {
        if (counter + value < 0)
            throw new CounterException("Counter overflow on addition.");

        counter += value;
    }

    /**
     * Resetuje licznik.
     */
    public void reset() {
        counter = 0;
    }

    @Override
    public int compareTo(Counter o) {
        return Float.compare(counter, o.counter);
    }
}
