package com.company.data;

import java.io.*;

/**
 * Obsługa strumieni audio.
 */
public class AudioResource {

    /**
     * Całkowita długość aktualnego utworu.
     */
    private int songTotalLength;
    /**
     * Lista plików audio.
     */
    File[] files;
    /**
     * Indeks wskazujący na aktualny plik audio w liście plików <code>files</code>
     */
    int currentFileIndex;
    /**
     * Strumień pliku audio.
     */
    FileInputStream fis;
    /**
     * Strumień pliku audio.
     */
    BufferedInputStream bis;

    /**
     * Pobiera listę plików z folderu 'music'.
     *
     * @throws AudioResourceException jeżeli pobranie utworów nie powiodło się
     */
    public AudioResource() throws AudioResourceException {
        files = new File(Resources.getURIToResource(".").resolve("music")).listFiles();
        if (files == null) {
            throw new AudioResourceException("Error while getting tracks. (not existing or empty 'music' directory)");
        }
        currentFileIndex = 0;

        createStreams(0);
    }

    /**
     * Tworzy strumień audio z pliku na który wskazuje indeks uwzględniając przesunięcie względem początku i go zwraca.
     *
     * @param skip przesunięcie względem początku
     * @return strumień audio
     * @throws AudioResourceException jeżeli otworzenie pliku nie powiodło się
     */
    public BufferedInputStream createStreams(long skip) throws AudioResourceException {
        try {
            fis = new FileInputStream(files[currentFileIndex]);
            songTotalLength = fis.available();
            fis.skip(skip);
            bis = new BufferedInputStream(fis);

        } catch (FileNotFoundException ex) {
            throw new AudioResourceException(ex.getMessage());
        } catch (IOException ex) {
            throw new AudioResourceException("Opening audio file failed: " + ex.getMessage());
        }

        return bis;
    }

    /**
     * Zwiększa indeks wskazujący na listę plików.
     * Jeżeli doszedł poza koniec to wraca na początek.
     */
    public void nextTrack() {
        currentFileIndex++;
        if (currentFileIndex >= files.length)
            currentFileIndex = 0;
    }

    /**
     * Zmniejsza indeks wskazujący na listę plików.
     * Nie przekracza początku.
     */
    public void prevTrack() {
        currentFileIndex--;
        if (currentFileIndex < 0)
            currentFileIndex = 0;
    }

    /**
     * Zwraca całkowitą wielkość strumienia aktualnego pliku audio.
     *
     * @return całkowita wielkość strumienia aktualnego pliku audio
     */
    public int getCurrentSongTotalLength() {
        return songTotalLength;
    }

    /**
     * Zwraca położenie początku strumienia audio.
     *
     * @return położenie początku strumienia audio
     */
    public int getCurrentPos() {
        int available = 0;
        try {
            available = fis.available();
        } catch (IOException ignored) {

        }

        return songTotalLength - available;
    }

    /**
     * Zwraca nazwę aktualnego pliku audio.
     *
     * @return nazwa aktualnego pliku audio
     */
    public String getCurrentTrackName() {
        return files[currentFileIndex].getName();
    }
}
