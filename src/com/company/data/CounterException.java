package com.company.data;

/**
 * Wyrzucony, gdy licznik ma nieprawidłową wartość (mniejszy od zera)
 */
public class CounterException extends Exception {

    /**
     * Konstruuje {@link CounterException} ze szczegółowym komunikatem.
     *
     * @param message szczegółowy komunikat
     */
    public CounterException(String message) {
        super(message);
    }
}
