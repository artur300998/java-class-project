package com.company.data.inout;

import com.company.data.State;

/**
 *  Klasa zajmująca się utrzymaniem połączenia z bazą i zlecająca zapis przebiegu podróży.
 */
public class JDBCLogManager {

    /**
     * Menedżer bazy danych.
     */
    private JDBCManager jdbcManager;

    /**
     * Otwarcie połączenia z bazą.
     *
     * @throws JDBCManagerException jeżeli połączenie z bazą nie powiedzie się
     */
    public JDBCLogManager() throws JDBCManagerException {
        this.jdbcManager = new JDBCManager();
    }

    /**
     * Zapisuje stan przebiegu podróży do bazy danych.
     *
     * @param state stan przebiegu podróży
     * @throws JDBCManagerException jeżeli zapis nie powiedzie się
     */
    public void logState(State state) throws JDBCManagerException {
        jdbcManager.insert(new LogTableRecord(state));
    }

    /**
     * Zamyka połączenie z bazą.
     */
    public void closeManager() {
        jdbcManager.closeConnection();
    }
}
