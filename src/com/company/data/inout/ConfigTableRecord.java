package com.company.data.inout;

/**
 * Reprezentuje rekord w tabeli zawierającej konfigurację.
 */
class ConfigTableRecord extends DBTableRecord {

    /**
     * Wartość dla kolumny 'propertyName'.
     */
    String propertyName;
    /**
     * Wartość dla kolumny 'propertyValue'.
     */
    Integer propertyValue;

    /**
     * Konstruuje obiekt klasy reprezentujący rekord w tabeli.
     *
     * @param propertyName  wartość dla kolumny 'propertyName'
     * @param propertyValue wartość dla kolumny 'propertyValue'
     */
    public ConfigTableRecord(String propertyName, Integer propertyValue) {
        super("Config");
        this.propertyName = propertyName;
        this.propertyValue = propertyValue;
    }

    @Override
    public String[] getColumnNamesInOrder() {
        return new String[]{"PropertyName", "PropertyValue"};
    }

    @Override
    public Object[] getColumnValuesInOrder() {
        return new Object[]{"'" + propertyName + "'", propertyValue};
    }
}
