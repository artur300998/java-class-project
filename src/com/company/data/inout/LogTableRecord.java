package com.company.data.inout;

import com.company.data.State;

/**
 * Reprezentuje rekord w tabeli z historią podróży.
 */
class LogTableRecord extends DBTableRecord {

    /**
     * Status podróży zawierający wartości dla odpowiednich kolumn.
     */
    State state;

    /**
     * Konstruuje obiekt klasy reprezentujący rekord w tabeli.
     *
     * @param state status podróży
     */
    public LogTableRecord(State state) {
        super("Log");
        this.state = state;
    }

    @Override
    public String[] getColumnNamesInOrder() {
        return new String[]{
                "speed", "totalCounter", "userCounter1", "userCounter2", "avgSpeed", "maxSpeed", "tripTime",
                "distance", "avgCombustion"
        };
    }

    @Override
    public Object[] getColumnValuesInOrder() {
        return new Object[]{
                state.speed,
                state.totalCounter,
                state.userCounter1,
                state.userCounter2,
                state.avgSpeed,
                state.maxSpeed,
                state.tripTime,
                state.distance,
                state.avgCombustion
        };
    }
}
