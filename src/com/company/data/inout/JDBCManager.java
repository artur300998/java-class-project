package com.company.data.inout;

import com.company.data.Resources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Obsługa bazy danych MS SQL z użyciem JDBC.
 */
class JDBCManager {

    /**
     * Połączenie z bazą danych.
     */
    Connection connection;

    /**
     * Próbuje zestawić połączenie z bazą danych.
     * W tym celu pozyskuje dane potrzebne do połączenia się z bazą z pliku konfiguracyjnego "dbconfig.properties".
     *
     * @throws JDBCManagerException jeżeli połączenie z bazą danych się nie powiodło
     */
    public JDBCManager() throws JDBCManagerException {
        String dbConfigFilename = "dbconfig.properties";
        String connectionString = Resources.getJDBCConnectionStringFromFile(dbConfigFilename);

        if (connectionString == null) {
            String errorMsg = dbConfigFilename + ": database connection configuration file not found.";

            try {
                Files.copy(Path.of(Resources.getURIToResource("default-" + dbConfigFilename)),
                        Path.of(Resources.getURIToResource(".").resolve(dbConfigFilename)), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new JDBCManagerException(errorMsg + "\nCouldn't copy the default one: " + e.getMessage());
            }

            throw new JDBCManagerException(errorMsg + "\nDefault one was copied, please edit it and try again.");
        }

        try {
            connection = DriverManager.getConnection(connectionString);
        } catch (SQLException e) {
            throw new JDBCManagerException("Can't connect to database: " + e.getMessage());
        }
    }

    /**
     * Inicjalizacja bazy danych danymi podanymi w argumencie.
     * UWAGA: usuwa dane jeśli jakiekolwiek znajdowały sie już w bazie.
     *
     *
     * @param records rekordy, którymi ma być zainicjowana tabela
     * @throws JDBCManagerException jeżeli dodanie rekordu nie powiodło się
     */
    public void initDB(ArrayList<? extends DBTableRecord> records) throws JDBCManagerException {
        deleteAll(records.get(0).getTableName());

        for (DBTableRecord record : records) {
            insert(record);
        }
    }

    /**
     * Odczytuje wszystkie dane z tabeli "Config" bazy danych i zwraca je.
     *
     * @return odczytane dane
     * @throws JDBCManagerException jeżeli pozyskanie danych nie powiedzie się
     */
    public ArrayList<ConfigTableRecord> readAllData() throws JDBCManagerException {
        ResultSet resultSet;
        String queryString = "SELECT * FROM Config";
        Statement statement;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(queryString);
        } catch (SQLException e) {
            throw new JDBCManagerException("Can't execute SQL query to read all data: " + e.getMessage());
        }

        ArrayList<ConfigTableRecord> data = new ArrayList<>();
        try {
            while (resultSet.next()) {
                data.add(new ConfigTableRecord(resultSet.getString("PropertyName"), resultSet.getInt("PropertyValue")));
            }

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            throw new JDBCManagerException(e.getMessage());
        }

        return data;
    }

    /**
     * Dodaje rekord do bazy danych.
     *
     * @param record rekord, który ma zostać dodany do bazy
     * @throws JDBCManagerException jeżeli dodanie rekordu nie powiedzie się
     */
    public void insert(DBTableRecord record) throws JDBCManagerException {
        String columnNamesStr = String.join(",", record.getColumnNamesInOrder());
        String columnValuesStr = Arrays.toString(record.getColumnValuesInOrder());
        columnValuesStr = columnValuesStr.replaceAll("[\\[\\]]", "");
        String queryString =
                "INSERT INTO " + record.getTableName() + " (" + columnNamesStr + ") VALUES (" + columnValuesStr + ")";
        executeUpdate(queryString);
    }

    /**
     * Usuwa rekord z bazy danych z tabeli "Config".
     *
     * @param propertyName wartość kolumny "PropertyName" dla której ma być usunięty rekord
     * @throws JDBCManagerException jeżeli operacja usuwania rekordu nie powiedzie się
     */
    public void delete(String propertyName) throws JDBCManagerException {
        String queryString =
                "DELETE FROM Config WHERE PropertyName='" + propertyName + "'";
        executeUpdate(queryString);
    }

    /**
     * Usunięcie wszystkich rekordów z tabeli.
     *
     * @param tableName nazwa tabeli dla której mają być usunięte rekordy
     * @throws JDBCManagerException jeżeli operacja usuwania nie powiedzie się
     */
    public void deleteAll(String tableName) throws JDBCManagerException {
        String queryString = "DELETE FROM " + tableName;
        executeUpdate(queryString);
    }

    /**
     * Aktualizuje rekord w tabeli "Config".
     *
     * @param propertyName wartość kolumny "PropertyName" dla której ma zostać zaktualizowana wartość
     * @param propertyValue wartość, która ma zostać nadpisana w miejsce istniejące w kolumnie "PropertyValue"
     * @throws JDBCManagerException jeżeli operacja nie powiedzie się
     */
    public void update(String propertyName, Integer propertyValue) throws JDBCManagerException {
        String queryString =
                "UPDATE Config SET PropertyValue=" + propertyValue + "WHERE PropertyName='" + propertyName + "'";
        executeUpdate(queryString);
    }

    /**
     * Wykonuje zapytanie aktualizujące podane w argumencie (INSERT, DELETE lub UPDATE).
     *
     * @param queryString zapytanie aktualizujące
     * @throws JDBCManagerException jeżeli operacja nie powiedzie się
     */
    private void executeUpdate(String queryString) throws JDBCManagerException {
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(queryString);
            statement.close();
        } catch (SQLException e) {
            throw new JDBCManagerException("Can't execute SQL query: " + e.getMessage());
        }
    }

    /**
     * Zamyka połaczenie z bazą danych.
     */
    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
