package com.company.data.inout;

/**
 * Wyrzucony, gdy wystąpił błąd menedżera baz danych.
 */
public class JDBCManagerException extends Exception {

    /**
     * Konstruuje {@link JDBCManagerException} ze szczegółowym komunikatem.
     *
     * @param message szczegółowy komunikat
     */
    public JDBCManagerException(String message) {
        super(message);
    }
}
