package com.company.data.inout;

import java.util.ArrayList;

/**
 * Implementacja metod zapisujących oraz odczytujących konfigurację z bazy danych MS SQL przez JDBC.
 */
public class JDBCConfigManager implements ConfigManager {

    @Override
    public Config getConfig() throws ConfigManagerException {
        JDBCManager jdbcManager;
        try {
            jdbcManager = new JDBCManager();
        } catch (JDBCManagerException e) {
            throw new ConfigManagerException("Error while connecting to MS SQL server: " + e.getMessage());
        }

        ArrayList<ConfigTableRecord> data;
        try {
            data = jdbcManager.readAllData();
        } catch (JDBCManagerException e) {
            throw new ConfigManagerException("Error while getting data from MS SQL server: " + e.getMessage());
        } finally {
            jdbcManager.closeConnection();
        }

        Config config;
        try {
            config = JDBCConfigHelper.getConfig(data);
        } catch (InvalidConfigException e) {
            throw new ConfigManagerException("Config is invalid: " + e.getMessage());
        }

        return config;
    }

    @Override
    public void saveConfig(Config config) throws ConfigManagerException {
        ArrayList<ConfigTableRecord> basicConfig = JDBCConfigHelper.getBasicConfig(config);

        JDBCManager jdbcManager;
        try {
            jdbcManager = new JDBCManager();
        } catch (JDBCManagerException e) {
            throw new ConfigManagerException("Error while connecting to MS SQL server: " + e.getMessage());
        }

        try {
            jdbcManager.initDB(basicConfig);
        } catch (JDBCManagerException e) {
            throw new ConfigManagerException("Error updating database: " + e.getMessage());
        } finally {
            jdbcManager.closeConnection();
        }
    }
}
