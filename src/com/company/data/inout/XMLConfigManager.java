package com.company.data.inout;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.XStreamException;
import com.thoughtworks.xstream.io.xml.StaxDriver;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementacja metod zapisujących oraz odczytujących konfigurację w pliku XML.
 */
public class XMLConfigManager implements ConfigManager {

    /**
     * Parser XML
     */
    XStream xStream;
    /**
     * menedżer plików XML
     */
    XMLFileManager XMLFileManager;

    /**
     * Konstruuje menedżer plików XMLowych oraz parser XML.
     *
     * @param filePath ścieżka do pliku, gdzie znajduje się lub ma się znajdować konfiguracja
     */
    public XMLConfigManager(String filePath) {
        XMLFileManager = new XMLFileManager(filePath);
        xStream = new XStream(new StaxDriver());
    }

    /**
     * Zmienia znaczniki XML, które będą generowane dla zapisywanych klas na niedomyślne.
     *
     * @param registrableConfig obiekt, dla którego mają być generowane niedomyślne znaczniki XML
     */
    public void registerAliases(RegistrableConfig registrableConfig) {
        HashMap<String, Class> aliases = registrableConfig.getAliases();
        for(Map.Entry<String, Class> mapEntry : aliases.entrySet()) {
            xStream.alias(mapEntry.getKey(), mapEntry.getValue());
        }
    }

    @Override
    public Config getConfig() throws ConfigManagerException {
        String xmlString = null;
        try {
            xmlString = XMLFileManager.readFromFile();
        }
        catch (IOException e) {
            throw new ConfigManagerException("Unable to read XML config file:\n" + e.getMessage());
        }

        assert xmlString != null;
        Config config = null;

        try {
            config = (Config)xStream.fromXML(xmlString);
        }
        catch (XStreamException e) {
            throw new ConfigManagerException("Unable to parse XML config file:\n" + e.getMessage());
        }

        try {
            config.validateConfig();
        } catch (InvalidConfigException e) {
            throw new ConfigManagerException("XML config file is invalid: " + e.getMessage());
        }

        return config;
    }

    @Override
    public void saveConfig(Config config) throws ConfigManagerException {
        String xmlString = xStream.toXML(config);

        try {
            XMLFileManager.saveToFile(xmlString);
        }
        catch (IOException e) {
            throw new ConfigManagerException("Unable to save config file.");
        }
    }
}
