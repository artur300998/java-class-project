package com.company.data.inout;

import java.util.HashMap;

/**
 * Interfejs dla klas, które chcą być zapisywane przez {@link com.thoughtworks.xstream.XStream}
 * do pliku XML ze nie domyślną nazwą znacznika.
 */
public interface RegistrableConfig {
    /**
     * Zwraca obiekty Class oraz odpowiadające im nazwy jako String.
     *
     * @return HashMap przechowujący pary String i Class.
     */
    HashMap<String, Class> getAliases();
}
