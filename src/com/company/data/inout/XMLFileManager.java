package com.company.data.inout;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Klasa zajmująca się odczytywaniem oraz zapisywaniem danych do plików XML używając String.
 */
class XMLFileManager extends File {

    /**
     * Wywołanie nadrzędnej klasy {@link File}.
     *
     * @param filePath ścieżka do pliku do/z którego ma być dokonywany zapis/odczyt
     */
    public XMLFileManager(String filePath) {
        super(filePath);
    }

    /**
     * Zapisuje do pliku String, który zawiera XML.
     *
     * @param xmlString łańcuch znaków zawierający XML
     * @throws IOException jeżeli zapis się nie powiedzie
     */
    public void saveToFile(String xmlString) throws IOException {
        try (FileWriter fileWriter = new FileWriter(this)) {
            fileWriter.write(xmlString);
        }
    }

    /**
     * Odczytuje łańcuch znaków z XML z pliku.
     *
     * @return łańcuch znaków z XML z pliku
     * @throws IOException jeżeli odczyt się nie powiedzie
     */
    public String readFromFile() throws IOException {
        char[] fileBuffer;

        FileReader fileReader = null;
        try {
            fileReader = new FileReader(this);
            fileBuffer = new char[(int) length()];
            fileReader.read(fileBuffer);
        }
        finally {
            if (fileReader != null) {
                fileReader.close();
            }
        }

        return new String(fileBuffer);
    }
}
