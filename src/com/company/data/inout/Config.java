package com.company.data.inout;

import com.company.data.Counter;
import com.company.data.CounterException;
import com.company.ui.gui.GUISettings;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Reprezentuje dane, które są zapisywane: liczniki oraz ustawienia graficzne.
 */
public class Config implements Serializable, RegistrableConfig {

    /**
     * Licznik
     */
    private Counter totalCounter, userCounter1, userCounter2;
    /**
     * Ustawienia graficzne prędkościomierza
     */
    private GUISettings guiSettings;

    /**
     * Wywołuje domyślne konstruktory przechowywanych obiektów.
     */
    public Config() {
        this.totalCounter = new Counter();
        this.userCounter1 = new Counter();
        this.userCounter2 = new Counter();
        this.guiSettings = new GUISettings();
    }

    /**
     * Ustawia liczniki.
     *
     * @param totalCounter stan licznika przebiegu całkowitego
     * @param userCounter1 stan licznika użytkownika
     * @param userCounter2 stan licznika użytkownika
     * @throws InvalidConfigException jeżeli podano nie prawidłową wartość dla licznika
     */
    public Config(int totalCounter, int userCounter1, int userCounter2) throws InvalidConfigException {
        try {
            this.totalCounter = new Counter(totalCounter);
            this.userCounter1 = new Counter(userCounter1);
            this.userCounter2 = new Counter(userCounter2);
        }
        catch (CounterException e) {
            throw new InvalidConfigException("Config is invalid:\n" + e.getMessage());
        }
    }

    /**
     * Ustawia ustawienia.
     *
     * @param guiSettings ustawienia
     */
    public void setGuiSettings(GUISettings guiSettings) {
        this.guiSettings = guiSettings;
    }

    /**
     * Zwraca licznik przebiegu całkowitego.
     *
     * @return licznik przebiegu całkowitego
     */
    public Counter getTotalCounter() {
        return totalCounter;
    }

    /**
     * Zwraca pierwszy licznik przebiegu użytkownika.
     *
     * @return pierwszy licznik przebiegu użytkownika
     */
    public Counter getUserCounter1() {
        return userCounter1;
    }

    /**
     * Zwraca drugi licznik przebiegu użytkownika.
     *
     * @return drugi licznik przebiegu użytkownika
     */
    public Counter getUserCounter2() {
        return userCounter2;
    }

    /**
     * Zwraca ustawienia
     *
     * @return ustawienia
     */
    public GUISettings getGuiSettings() {
        return guiSettings;
    }

    /**
     * Sprawdza poprawność przechowywanych przez liczniki wartości.
     *
     * @throws InvalidConfigException jeżeli licznik przechowuje nie prawidłową wartość
     */
    public void validateConfig() throws InvalidConfigException {
        try {
            totalCounter.validateCounter();
            userCounter1.validateCounter();
            userCounter2.validateCounter();
        }
        catch (CounterException e) {
            throw new InvalidConfigException("Config is invalid:\n" + e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Config{" +
                "totalCounter=" + totalCounter +
                ", userCounter1=" + userCounter1 +
                ", userCounter2=" + userCounter2 +
                ", settings=" + guiSettings +
                '}';
    }

    @Override
    public HashMap<String, Class> getAliases() {
        HashMap<String, Class> aliases = new HashMap<>();

        aliases.put(Config.class.getSimpleName(), Config.class);
        aliases.put(Counter.class.getSimpleName(), Counter.class);
        aliases.put(GUISettings.class.getSimpleName(), GUISettings.class);

        return aliases;
    }
}
