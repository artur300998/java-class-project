package com.company.data.inout;

/**
 *  Klasa bazowa dla klas reprezentujących rekord w tabeli.
 */
abstract class DBTableRecord {

    /**
     * Nazwa tabeli, której rekord dotyczy
     */
    private String tableName;

    /**
     * Konstruuje obiekt klasy, który będzie reprezentować rekord w tabeli.
     *
     * @param tableName nazwa tabeli, które dotyczyć będzie rekord
     */
    public DBTableRecord(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Zwraca nazwy kolumn tabeli w odpowiedniej kolejności zgodnej
     * z wartościami zwracanymi przez metodę <code>getColumnValuesInOrder</code>.
     *
     * @return nazwy kolumn tabeli jako tabela {@link String}
     */
    abstract String[] getColumnNamesInOrder();

    /**
     * Zwraca wartości rekordu tabeli dla jej kolumn w koljeności zgodnej z
     * zwracanymi przez metodę <code>getColumnNamesInOrder</code> nazwami kolumn.
     *
     * @return wartości dla rekordu tabeli
     */
    abstract Object[] getColumnValuesInOrder();

    /**
     * Zwraca nazwę tabeli, której tyczy się dany rekord
     *
     * @return nazwa tabeli
     */
    String getTableName() {
        return tableName;
    }
}
