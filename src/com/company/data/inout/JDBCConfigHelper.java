package com.company.data.inout;

import com.company.ui.gui.GUISettings;

import java.util.ArrayList;
import java.util.Map;

/**
 * Pomocnicza klasa dla {@link JDBCConfigManager}, której metody zamieniają obiekt klasy {@link Config}
 * na {@link ArrayList} ze wszystkimi danymi do zapisania i na odwrót.
 */
class JDBCConfigHelper {

    /**
     * Zwraca {@link ArrayList} z całą konfiguracją dla podanego w argumencie obiektu {@link Config}.
     *
     * @param config konfiguracja, która zostanie zamieniona na {@link ArrayList}
     * @return {@link ArrayList} z całą konfiguracją
     */
    static public ArrayList<ConfigTableRecord> getBasicConfig(Config config) {
        ArrayList<ConfigTableRecord> basicConfig = new ArrayList<>();

        basicConfig.add(new ConfigTableRecord("totalCounter", config.getTotalCounter().getValue()));
        basicConfig.add(new ConfigTableRecord("userCounter1", config.getUserCounter1().getValue()));
        basicConfig.add(new ConfigTableRecord("userCounter2", config.getUserCounter2().getValue()));
        for(Map.Entry<String, Integer> entry : config.getGuiSettings().getAllSettings().entrySet()) {
            basicConfig.add(new ConfigTableRecord(entry.getKey(), entry.getValue()));
        }

        return basicConfig;
    }

    /**
     * Zwraca obiekt klasy {@link Config} zawierający dane zawarte w {@link ArrayList} podanym w argumencie.
     *
     * @param basicConfig {@link ArrayList} zawierający dane konfiguracji
     * @return obiekt konfiguracji
     * @throws InvalidConfigException jeżeli walidacja konfiguracji się nie powiodła (nie prawidłowe dane)
     */
    static public Config getConfig(ArrayList<ConfigTableRecord> basicConfig) throws InvalidConfigException {

        int totalCounter = 0, userCounter1 = 0, userCounter2 = 0;
        GUISettings guiSettings = new GUISettings();
        for (ConfigTableRecord record : basicConfig) {
            if (record.propertyName.equals("totalCounter"))
                totalCounter = record.propertyValue;
            else if (record.propertyName.equals("userCounter1"))
                userCounter1 = record.propertyValue;
            else if (record.propertyName.equals("userCounter2"))
                userCounter2 = record.propertyValue;
            else
                guiSettings.addSetting(record.propertyName, record.propertyValue);
        }

        Config config = new Config(totalCounter, userCounter1, userCounter2);
        config.setGuiSettings(guiSettings);

        return config;
    }
}
