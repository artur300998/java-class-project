package com.company.data.inout;

/**
 * Wyrzucony podczas nie powodzenia działania klasy implementującej {@link ConfigManager} (odczyt/zapis konfiguracji).
 */
public class ConfigManagerException extends Exception {

    /**
     * Konstruuje {@link ConfigManagerException} ze szczegółowym komunikatem.
     *
     * @param message szczegółowy komunikat
     */
    public ConfigManagerException(String message) {
        super(message);
    }
}
