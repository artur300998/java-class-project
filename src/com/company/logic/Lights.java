package com.company.logic;

import java.util.HashMap;

/**
 * Reprezentuje światła na desce rozdzielczej.
 * Zawiera stany wszystkich przechowywanych świateł.
 * Światła pozycyjne, mijania oraz drogowe mogą być przełączne tylko w cyklu:
 * pozycjne - mijania - drogowe
 * Światła przeciwmgłowe mogą być przełączane dowolnie.
 * Światła kierunkowskazów spełniają warunek, że tylko jeden z nich jest aktywny w danym momencie.
 * (włączenie jednego, gasi drugi)
 */
public class Lights {

    /**
     * Typy świateł.
     */
    public enum LightType {
        LOWBEAM_HEADLIGHT,
        HIGHBEAM_HEADLIGHT,
        POSITION_LIGHT,
        FOG_FRONT,
        FOG_BACK,
        LEFT_INDICATOR,
        RIGHT_INDICATOR
    }

    /**
     * Stany świateł.
     */
    HashMap<LightType, Boolean> lightsStates;

    /**
     * Ustawia stany wszystkich świateł na zgaszone.
     */
    public Lights() {
        lightsStates = new HashMap<>();
        lightsStates.put(LightType.LOWBEAM_HEADLIGHT, false);
        lightsStates.put(LightType.HIGHBEAM_HEADLIGHT, false);
        lightsStates.put(LightType.POSITION_LIGHT, false);
        lightsStates.put(LightType.FOG_FRONT, false);
        lightsStates.put(LightType.FOG_BACK, false);
        lightsStates.put(LightType.LEFT_INDICATOR, false);
        lightsStates.put(LightType.RIGHT_INDICATOR, false);
    }

    /**
     * Zwraca stan danego światła.
     *
     * @param lightType typ światła dla którego ma być zwrócony stan
     * @return stan światła
     */
    public boolean getLightState(LightType lightType) {
        return lightsStates.get(lightType);
    }

    /**
     * Cykliczne przełączanie świateł: pozycyjne - mijania - drogowe.
     *
     * @param up "true" - cykl w prawo, "false" - cykl w lewo
     */
    public void cycleHeadlightsState(boolean up) {
        if (getLightState(LightType.HIGHBEAM_HEADLIGHT)) {
            if (!up)
                lightsStates.put(LightType.HIGHBEAM_HEADLIGHT, false);
        }
        else if (getLightState(LightType.LOWBEAM_HEADLIGHT)) {
            if (!up) {
                lightsStates.put(LightType.LOWBEAM_HEADLIGHT, false);
                lightsStates.put(LightType.POSITION_LIGHT, true);
            }
            else
                lightsStates.put(LightType.HIGHBEAM_HEADLIGHT, true);
        }
        else if (getLightState(LightType.POSITION_LIGHT)) {
            if (!up)
                lightsStates.put(LightType.POSITION_LIGHT, false);
            else {
                lightsStates.put(LightType.POSITION_LIGHT, false);
                lightsStates.put(LightType.LOWBEAM_HEADLIGHT, true);
            }
        }
        else {
            if (up)
                lightsStates.put(LightType.POSITION_LIGHT, true);
        }
    }

    /**
     * Przełącza światła przeciwmgłowe.
     *
     * @param lightType typ światła, który ma być przełączony (inne niż przeciwmgłowe są ignorowane)
     */
    public void switchLightState(LightType lightType) {
        switch (lightType) {
            case FOG_FRONT:
                lightsStates.put(LightType.FOG_FRONT, !getLightState(LightType.FOG_FRONT));
                break;
            case FOG_BACK:
                lightsStates.put(LightType.FOG_BACK, !getLightState(LightType.FOG_BACK));
                break;
        }
    }

    /**
     * Włącza kierunkowskaz. Wyłącza drugi jeśli był zapalony.
     *
     * @param lightType kierunkowskaz do aktywowania (inne typy świateł są ignorowane)
     */
    public void enableIndicator(LightType lightType) {
        switch(lightType) {
            case LEFT_INDICATOR: {
                lightsStates.put(LightType.LEFT_INDICATOR, true);
                lightsStates.put(LightType.RIGHT_INDICATOR, false);
            }
            break;
            case RIGHT_INDICATOR: {
                lightsStates.put(LightType.RIGHT_INDICATOR, true);
                lightsStates.put(LightType.LEFT_INDICATOR, false);
            }
            break;
        }
    }

    /**
     * Wyłącza kierunkowskaz
     *
     * @param lightType kierunkowskaz do dezaktywowania (inne typy świateł są ignorowane)
     */
    public void disableIndicator(LightType lightType) {
        if (lightType == LightType.LEFT_INDICATOR || lightType == LightType.RIGHT_INDICATOR)
        lightsStates.put(lightType, false);
    }
}
