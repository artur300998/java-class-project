package com.company.logic;

import com.company.data.AudioResource;
import com.company.data.AudioResourceException;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 * Klasa obsługująca radio.
 */
public class AudioPlayer {
    /**
     * Odtwarzacz.
     */
    private Player player;

    /**
     * Lokalizacja w strumieniu audio zatrzymania się ostatniego odtwarzania.
     */
    private long pauseLocation;

    /**
     * Wartość logiczna oznaczająca stan odtwarzania.
     */
    private boolean isPlaying = false;

    /**
     * Obsługuje strumienie audio.
     */
    private AudioResource audioResource;

    /**
     * Konstruuje obiekt dokonując inicjalizacji.
     */
    public AudioPlayer() {
        init();
    }

    /**
     * Inicjalizacja radia.
     */
    public void init() {
        try {
            audioResource = new AudioResource();
            pauseLocation = 0;
        } catch (AudioResourceException e) {
            e.printStackTrace();
        }
    }

    /**
     * Odtwarza wczytany utwór zaczynając od zapisanego czasu utworu
     */
    public void playAudio() {
        try {
            player = new Player(audioResource.createStreams(pauseLocation));
        } catch (JavaLayerException | AudioResourceException e) {
            e.printStackTrace();
        }

        new Thread() {
            @Override
            public void run() {
                try {
                    isPlaying = true;
                    player.play();
                    if (player.isComplete())
                        nextSong();
                } catch (JavaLayerException ex) {
                    ex.printStackTrace();
                }
            }

        }.start();
    }

    /**
     * Wywołuje <code>stopAudio</code> z zapisaniem momentu zatrzymania odtwarzania.
     */
    public void pauseAudio() {
        pauseLocation = stopAudio();
    }

    /**
     * Zamyka strumień i ustawia czas utworu na początek.
     *
     * @return moment zatrzymania odtwarzania
     */
    public int stopAudio() {
        int stopLocation = audioResource.getCurrentPos();
        if (player != null) {
            player.close();
        }
        pauseLocation = 0;
        isPlaying = false;

        return stopLocation;
    }

    /**
     * Wczytuje następny utwór i go odtwarza, jeżeli do tej pory odtwarzacz działał.
     */
    public void nextSong() {
        boolean wasPlaying = isPlaying;
        stopAudio();
        audioResource.nextTrack();
        if (wasPlaying)
            playAudio();
    }

    /**
     * Wczytuje poprzedni utwór i go odtwarza, jeżeli do tej pory odtwarzacz działał.
     */
    public void prevSong() {
        boolean wasPlaying = isPlaying;
        stopAudio();
        audioResource.prevTrack();
        if (wasPlaying)
            playAudio();
    }

    /**
     * Zwraca procent utworu, który upłynął.
     *
     * @return procent, który upłynął
     */
    public int getSongPercent() {
        int percent = 0;
        int songTotalLength = audioResource.getCurrentSongTotalLength();
        if (songTotalLength != 0)
            percent = (int) (100 * getCurrentTrackPos() / songTotalLength);

        return percent;
    }

    /**
     * Zwraca aktualną pozycję odtwarzania.
     *
     * @return aktualna pozycja odtwarzania
     */
    public long getCurrentTrackPos() {
        long pos = 0;
        if (isPlaying) {
            pos = audioResource.getCurrentPos();
        } else {
            pos = pauseLocation;
        }

        return pos;
    }

    /**
     * Zwraca nazwę aktualnie odtwarzanego pliku.
     *
     * @return nazwa aktualnie odtwarzanego pliku
     */
    public String getCurrentTrackName() {
        return audioResource.getCurrentTrackName();
    }

    /**
     * Zwraca wartość logiczną, która oznacza, czy odtwarzacz działa.
     *
     * @return czy odtwarzacz działa
     */
    public boolean isPlaying() {
        return isPlaying;
    }
}
