package com.company.logic;

/**
 * Główna klasa logiki. Uruchamia deskę rozdzielczą.
 * Klasa wykorzystuje wzorzec projektowy "Singleton".
 * Wykorzystywana jest w warstwie interfejsu użytkownika.
 */
public class MainLogic implements Runnable {

    /**
     * Jedyny obiekt tej klasy.
     */
    static private MainLogic mainLogic;
    static {
        mainLogic = new MainLogic();
    }

    /**
     * Deska rozdzielcza.
     */
    private Dashboard dashboard;

    /**
     * Inicjalizacja deski rozdzielczej.
     */
    private MainLogic() {
        dashboard = new Dashboard();
    }

    /**
     * Zwraca jedyny obiekt tej klasy.
     *
     * @return obiekt tej klasy
     */
    static public MainLogic getInstance() {
        return mainLogic;
    }

    /**
     * Zwraca obiekt reprezentujący deskę rozdzielczą.
     *
     * @return obiekt reprezentujący deskę rozdzielczą
     */
    public Dashboard getDashboard() {
        return dashboard;
    }

    /**
     * Uruchamia deskę rozdzielczą (logikę).
     */
    @Override
    public void run() {
        dashboard.run();
    }
}
