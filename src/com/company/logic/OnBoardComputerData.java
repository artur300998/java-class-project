package com.company.logic;

/**
 * Klasa przechowująca dane komputera pokładowego.
 */
public class OnBoardComputerData {
    /**
     * Średnia prędkość (km/h).
     */
    public float avgSpeed = 0;
    /**
     * Maksymalna prędkość (km/h).
     */
    public float maxSpeed = 0;
    /**
     * Czas podróży (min).
     */
    public int tripTime = 0;
    /**
     * Przebyty dystans (km).
     */
    public float distance = 0;
    /**
     * Średnie spalanie (l/100km).
     */
    public float avgCombustion = 0;

    /**
     * Zwraca dane z komputera pokładowego w postaci łańcucha znaków
     *
     * @return dane z komputera pokładowego
     */
    public String toString()
    {
        String text = "Prędkość średnia: " + Float.toString(avgSpeed) + " km/h" +
                "\nPrędkość maksymalna: " + Float.toString(maxSpeed) + " km/h" +
                "\nCzas podróży: " + Integer.toString(tripTime) + " min" +
                "\nDystans: " + Float.toString(distance) + " km" +
                "\nSrednie spalanie: " + Float.toString(avgCombustion) + " l/100km";

        return text;
    }
}



