package com.company.logic;

import com.company.data.CounterException;
import com.company.data.DataRepository;
import com.company.data.State;

import java.time.Duration;
import java.time.Instant;

/**
 * Wykonywanie głównego programu.
 */
public class Dashboard {

    /**
     * Status działania programu.
     */
    private enum State {
        RUNNING,
        STOPPING,
        STOPPED
    }
    /**
     * Aktualny status działania programu.
     */
    private State state;

    /**
     * Stan przyspieszania.
     */
    public enum Accel {
        UP,
        NOT_UP,
        DOWN,
        NOT_DOWN,
        NONE
    }
    /**
     * Aktualny stan przyspieszania.
     */
    private Accel accel;

    /**
     * Biegi
     */
    public enum Gear {
        N('N'), FIRST('1'), SECOND('2'), THIRD('3'), FOURTH('4'), FIFTH('5'), SIXTH('6');

        /**
         * Symbol biegu.
         */
        private char name;

        /**
         * Tworzy obiekt typu wyliczoniowego.
         *
         * @param name symbol biegu
         */
        Gear(char name) {
            this.name = name;
        }

        /**
         * Zwraca symbol biegu.
         *
         * @return symbol biegu
         */
        public char getAsChar() {
            return name;
        }
    }

    /**
     * Aktualny bieg.
     */
    private Gear gear;

    /**
     * Operacje zapisu, odczytu.
     */
    private DataRepository dataRepository;

    /**
     * Światła.
     */
    private Lights lights;

    /**
     * Radio MP3.
     */
    private AudioPlayer audioPlayer;

    /**
     * Czy tempomat jest aktywny.
     */
    private boolean isCruiseControlActive = false;

    /**
     * Aktualna prędkość.
     */
    private float speed = 0;

    /**
     * Maksymalna możliwa do osiągnięcia prędkość.
     */
    private float maxSpeed = 200;

    /**
     * Docelowa prędkość tempomatu.
     */
    private float cruiseControlTargetSpeed = 0;

    /**
     * Tolerancja tempomatu.
     */
    final private float cruiseControlSpeedTolerance = 1.0f;

    /**
     * Dane komputera pokładowego.
     */
    private OnBoardComputerData onBoardComputerData;

    /**
     * Czas działania w ns.
     */
    private long totalTimeNs = 1;

    /**
     * Zużyte paliwo.
     */
    private float usedFuel = 0;

    /**
     * Inicjalizacja deski rozdzielczej i jej komponentów.
     */
    public Dashboard() {
        dataRepository = new DataRepository();
        lights = new Lights();
        audioPlayer = new AudioPlayer();

        onBoardComputerData = new OnBoardComputerData();

        state = State.RUNNING;
        accel = Accel.NONE;
        gear = Gear.N;
    }

    /**
     * Wykonuje główny algorytm.
     */
    public void run() {
        Instant startInstant = Instant.now();
        Instant stateSaveStartInstant = Instant.now();
        while(state == State.RUNNING) {
            switch (accel) {
                case UP: {
                    accelerate();
                }
                    break;
                case NONE: {
                    if (isCruiseControlActive) {
                        if (speed < cruiseControlTargetSpeed - cruiseControlSpeedTolerance) {
                            accelerate();
                        } else if (speed > cruiseControlTargetSpeed + cruiseControlSpeedTolerance) {
                            decelerate();
                        }
                    } else {
                        slowDown();
                    }
                }
                    break;
                case DOWN: {
                    decelerate();
                }
                    break;
            }

            usedFuel += fuelUsage();
            checkSpeedAndChangeGear();

            Instant endInstant = Instant.now();
            Duration duration = Duration.between(startInstant, endInstant);
            long deltaTimeNs = duration.getSeconds() * 1000000000 + duration.getNano();
            totalTimeNs += deltaTimeNs;

            calculateOnBoardComputerData(deltaTimeNs);
            addToCounters(deltaTimeNs);

            startInstant = endInstant;

            if (Duration.between(stateSaveStartInstant, Instant.now()).getNano() > 500000000) {
                stateSaveStartInstant = Instant.now();

                com.company.data.State state = new com.company.data.State();
                state.speed = speed;
                state.totalCounter = dataRepository.getTotalCounterValue();
                state.userCounter1 = dataRepository.getUserCounter1Value();
                state.userCounter2 = dataRepository.getUserCounter2Value();
                state.avgSpeed = onBoardComputerData.avgSpeed;
                state.maxSpeed = onBoardComputerData.maxSpeed;
                state.tripTime = onBoardComputerData.tripTime;
                state.distance = onBoardComputerData.distance;
                state.avgCombustion = onBoardComputerData.avgCombustion;

                dataRepository.saveStateToDatabaseLog(state);
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        dataRepository.closeRepository();

        state = State.STOPPED;
        System.out.println("Stopped.");
    }

    /**
     * Zatrzymuje wykonywanie programu.
     */
    public void stop() {
        System.out.println("Stopping...");
        state = State.STOPPING;
        while (state != State.STOPPED) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Zwraca współczynnik przyspieszenia w zależności od aktualnego biegu.
     *
     * @return współczynnik
     */
    private double accelerationValue()
    {
        if (gear == Gear.SIXTH)
            return 0.3;
        else if(gear == Gear.FIFTH)
            return 0.4;
        else if (gear == Gear.FOURTH)
            return 0.5;
        else if (gear == Gear.THIRD)
            return 0.6;
        else if (gear == Gear.SECOND)
            return 0.7;
        else
            return 0.8;

    }

    /**
     * Wykonuje operację przyspieszania.
     */
    private void accelerate() {
        double accelerationVal = accelerationValue();
        if(speed + accelerationVal <= maxSpeed)
            speed += accelerationVal;
    }

    /**
     * Wykonuje operację zwalniania.
     */
    private void slowDown() {
        double slowDownRate = accelerationValue()*0.5;
        if(speed - slowDownRate >= 0)
            speed -= slowDownRate;
        else
            speed = 0;
    }

    /**
     * Wykonuje operację hamowania.
     */
    private void decelerate() {
        speed -= 0.7f;
        if (speed < 0)
            speed = 0.0f;
    }

    /**
     * Oblicza zużycie paliwa.
     *
     * @return zużycie paliwa
     */
    private float fuelUsage()
    {
        if(accel == Accel.UP || accel == Accel.DOWN)
            return speed / 150;
        else
            return speed / 300;
    }

    /**
     * Oblicza dane komputera pokładowego.
     * @param timeDeltaNs różnica czasu w ns
     */
    private void calculateOnBoardComputerData(long timeDeltaNs) {
        onBoardComputerData.distance += speed * (timeDeltaNs / (1000000000.0f * 3600));
        onBoardComputerData.tripTime = (int) (totalTimeNs / (1000000000L * 60));
        if (totalTimeNs > 0)
            onBoardComputerData.avgSpeed = onBoardComputerData.distance / (totalTimeNs / (1000000000.0f * 3600));
        onBoardComputerData.maxSpeed = Math.max(onBoardComputerData.maxSpeed, speed);
        if (onBoardComputerData.distance > 0)
            onBoardComputerData.avgCombustion = usedFuel / (100 * onBoardComputerData.distance);
    }

    /**
     * Dodaje do liczników.
     * @param timeDeltaNs różnica czasu w ns
     */
    private void addToCounters(long timeDeltaNs)
    {
        float deltaDistance = speed * timeDeltaNs / (1000000.0f * 3600);
        try {
            getDataRepository().addToTotalCounter(deltaDistance);
            getDataRepository().addToUserCounter1(deltaDistance);
            getDataRepository().addToUserCounter2(deltaDistance);
        }
        catch (CounterException e) {
            e.printStackTrace();
        }
    }

    /**
     * Zmienia bieg w zależności od prędkości.
     */
    private void checkSpeedAndChangeGear() {
        if (speed > 150)
            gear = Gear.SIXTH;
        else if(speed > 90)
            gear = Gear.FIFTH;
        else if (speed > 50)
            gear = Gear.FOURTH;
        else if (speed > 30)
            gear = Gear.THIRD;
        else if (speed > 15)
            gear = Gear.SECOND;
        else if (speed > 1)
            gear = Gear.FIRST;
        else
            gear = Gear.N;
    }

    /**
     * Zwraca aktualną prędkość.
     *
     * @return aktualna prędkość w km/h
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * Zwraca prędkość maksymalną.
     *
     * @return prędkość maksymalna w km/h
     */
    public float getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * Zwraca dane, które są zapisywane (np. liczniki).
     *
     * @return dane, które są zapisywane
     */
    public DataRepository getDataRepository() {
        return dataRepository;
    }

    /**
     * Zwraca stan świateł.
     *
     * @return stan świateł
     */
    public Lights getLights() {
        return lights;
    }

    /**
     * Zwraca radio.
     *
     * @return radio
     */
    public AudioPlayer getAudioPlayer() {
        return audioPlayer;
    }

    /**
     * Zwraca dane komputera pokładowego.
     *
     * @return dane komputera pokładowego
     */
    public OnBoardComputerData getOnBoardComputerData() {
        return onBoardComputerData;
    }

    /**
     * Zwraca aktualny bieg.
     *
     * @return aktualny bieg
     */
    public Gear getGear() {
        return gear;
    }

    /**
     * Ustawia stan przyspieszenia.
     *
     * @param accel stan przyspieszenia
     */
    public void setAccel(Accel accel) {
        isCruiseControlActive = false;
        if ((accel == Accel.NOT_DOWN && this.accel == Accel.DOWN) || (accel == Accel.NOT_UP && this.accel == Accel.UP))
            this.accel = Accel.NONE;
        else if (accel == Accel.UP || accel == Accel.DOWN)
            this.accel = accel;
    }

    /**
     * Przełącza stan aktywacji tempomatu.
     */
    public void switchCruiseControlActivation() {
        isCruiseControlActive = !isCruiseControlActive;
        if (isCruiseControlActive)
            cruiseControlTargetSpeed = speed;
    }

    /**
     * Dodaje prędkość do tempomatu (+5 km/h).
     */
    public void cruiseControlAddSpeed() { cruiseControlTargetSpeed += 5; }

    /**
     * Odejmuje prędkość od tempomatu (-5 km/h).
     */
    public void cruiseControlRemoveSpeed() {
        cruiseControlTargetSpeed -= 5;
    }

    /**
     * Zwraca wartość logiczną oznaczającą stan aktywacji tempomatu.
     *
     * @return stan aktywacji tempomatu
     */
    public boolean isCruiseControlActive() {
        return isCruiseControlActive;
    }
}
